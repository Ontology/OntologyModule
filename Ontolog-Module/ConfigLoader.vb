﻿Imports OntologyClasses.BaseClasses
Imports OntologyClasses.DataClasses
Imports OntologyAppDBConnector

Module ConfigLoader
    Public Function LoadGlobalConfig() As Globals
        Dim boolConfig = False
        Dim objLogstates As New clsLogStates
        Dim objGlobals As Globals = Nothing
        While boolConfig = False
            Try
                objGlobals = New Globals(True)
                boolConfig = True
            Catch ex As Exception
                Dim objFrmConfig = New frmConfig(Nothing)
                objFrmConfig.ShowDialog()
                Select Case objFrmConfig.Result.GUID
                    Case objLogstates.LogState_Error.GUID
                        Environment.Exit(-1)
                    Case objLogstates.LogState_Nothing.GUID
                        Environment.Exit(0)

                End Select

            End Try
        End While

        Return objGlobals
    End Function

    
End Module
