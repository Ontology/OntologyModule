﻿Imports Ontology_Module
Imports OntologyClasses.BaseClasses
Imports System.Globalization
Imports OntologyAppDBConnector

Public Class clsLocalizeGui
    Private localConfig As IGuiLocalization

    Private objDBLevel_GuiItems_L1 As OntologyModDBConnector
    Private objDBLevel_GuiItems_L2 As OntologyModDBConnector
    Private objDBLevel_GuiCaption As OntologyModDBConnector
    Private objDBLevel_ToolTips As OntologyModDBConnector
    Private objDBLevel_Messages As OntologyModDBConnector
    Private objDBLevel_LocalizedMessages As OntologyModDBConnector
    Private objDBLevel_Languages As OntologyModDBConnector
    Private objDBLevel_LanguageShort As OntologyModDBConnector
    Private objDBLevel_Caption As OntologyModDBConnector
    Private objDBLevel_Message As OntologyModDBConnector

    Public Sub New(localConfig As IGuiLocalization)
        Me.localConfig = localConfig

        Initialize()
    End Sub

    Public Sub ConfigureControlsLanguage(parentControl As Control, frmName As String)
        If TypeOf (parentControl) Is Form Or TypeOf (parentControl) Is UserControl Then
            frmName = parentControl.Name
        End If

        Dim caption = GetGuiCaption(frmName, _
                                        parentControl.Name, _
                                        CultureInfo.CurrentCulture.TwoLetterISOLanguageName)

        parentControl.Text = If(Not String.IsNullOrEmpty(caption), caption, parentControl.Text)

        For Each subControl As Control In parentControl.Controls

            If Not TypeOf (subControl) Is UserControl Then
                caption = GetGuiCaption(frmName, _
                                        subControl.Name, _
                                        CultureInfo.CurrentCulture.TwoLetterISOLanguageName)
                If Not String.IsNullOrEmpty(caption) Then
                    Dim controlType = subControl.GetType()
                    Dim propertyItem = controlType.GetProperty("Text")

                    If Not propertyItem Is Nothing Then
                        propertyItem.SetValue(subControl, caption)

                    Else
                        propertyItem = controlType.GetProperty("Caption")
                        If Not propertyItem Is Nothing Then
                            propertyItem.SetValue(subControl, caption)

                        End If
                    End If
                End If

                ConfigureControlsLanguage(subControl, frmName)

                If TypeOf (subControl) Is MenuStrip Then
                    Dim menuStrip = CType(subControl, MenuStrip)
                    For Each subMenuItem As ToolStripMenuItem In menuStrip.Items
                        LocalizeMenuItems(subMenuItem, frmName)
                    Next
                End If

                If TypeOf (subControl) Is StatusStrip Then
                    Dim menuStrip = CType(subControl, StatusStrip)
                    For Each subMenuItem As Object In menuStrip.Items
                        
                        LocalizeObjects(subMenuItem, frmName)



                    Next
                End If

                If TypeOf (subControl) Is ToolStrip Then
                    Dim menuStrip = CType(subControl, ToolStrip)
                    For Each subMenuItem As Object In menuStrip.Items

                        LocalizeObjects(subMenuItem, frmName)



                    Next
                End If

                If Not subControl.ContextMenuStrip Is Nothing Then
                    Dim objContextMenu = subControl.ContextMenuStrip

                    For Each objToolStripMenuItem As ToolStripMenuItem In objContextMenu.Items
                        LocalizeMenuItems(objToolStripMenuItem, frmName)
                    Next
                End If
            End If

        Next




    End Sub

    
    Private Sub LocalizeObjects(objItem As Object, frmName As String)
        Dim controlType = objItem.GetType()
        Dim propertyItem = controlType.GetProperty("Name")

        If Not propertyItem Is Nothing Then
            Dim itemName = propertyItem.GetValue(objItem)

            Dim caption = GetGuiCaption(frmName, _
                        itemName.ToString(), _
                        CultureInfo.CurrentCulture.TwoLetterISOLanguageName)

            If Not String.IsNullOrEmpty(caption) Then
                propertyItem = controlType.GetProperty("Text")

                If Not propertyItem Is Nothing Then
                    propertyItem.SetValue(objItem, caption)

                Else
                    propertyItem = controlType.GetProperty("Caption")
                    If Not propertyItem Is Nothing Then
                        propertyItem.SetValue(objItem, caption)

                    End If
                End If
            End If

        End If
    End Sub

    Private Sub LocalizeMenuItems(menuItem As ToolStripMenuItem, frmName As String)
        Dim caption = GetGuiCaption(frmName, _
                                    menuItem.Name, _
                                    CultureInfo.CurrentCulture.TwoLetterISOLanguageName)

        menuItem.Text = If(Not String.IsNullOrEmpty(caption), caption, menuItem.Text)

        For Each subMenuItem As ToolStripMenuItem In menuItem.DropDownItems
            LocalizeMenuItems(subMenuItem, frmName)
        Next
    End Sub


    Public Function GetGuiCaption(NameForm As String, NameGuiEntry As String, languageShort As String) As String
        Dim caption = ""
        Dim objForms = objDBLevel_GuiItems_L1.ObjectRels.Where(Function(form) form.Name_Other.ToLower() = NameForm.ToLower()).ToList()

        Dim objGuiEntries = New List(Of clsObjectRel)()

        If (Not NameGuiEntry = NameForm) Then
            objGuiEntries = (From objForm In objForms
                             Join objGuiEntry In objDBLevel_GuiItems_L2.ObjectRels On objForm.ID_Other Equals objGuiEntry.ID_Object
                             Where objGuiEntry.Name_Other.ToLower() = NameGuiEntry.ToLower()
                             Select objGuiEntry).ToList()

        Else
            objGuiEntries = objForms
        End If

        Dim objGuiCaptions = (From objGuiEntry In objGuiEntries
                              Join objGuiCaption In objDBLevel_GuiCaption.ObjectRels On objGuiEntry.ID_Other Equals objGuiCaption.ID_Object
                              Join objLanguage In objDBLevel_Languages.ObjectRels On objGuiCaption.ID_Other Equals objLanguage.ID_Object
                              Join objShort In objDBLevel_LanguageShort.ObjAtts On objLanguage.ID_Other Equals objShort.ID_Object
                              Where objShort.Val_String.ToLower() = languageShort.ToLower()
                              Select objGuiCaption.Name_Other).ToList()

        If objGuiCaptions.Any() Then
            caption = objGuiCaptions.First()
        End If

        Return caption
    End Function

    Public Function GetToolTip(NameForm As String, NameGuiEntry As String, languageShort As String) As String
        Dim toolTip = ""

        Return toolTip
    End Function

    Public Function GetUserMessageCaption(NameForm As String, NameGuiEntry As String, languageShort As String) As String
        Dim caption = ""

        Return caption
    End Function

    Public Function GetInputMessageCaption(NameForm As String, NameGuiEntry As String, languageShort As String) As String
        Dim caption = ""

        Return caption
    End Function

    Public Function GetErrorMessageCaption(NameForm As String, NameGuiEntry As String, languageShort As String) As String
        Dim caption = ""

        Return caption
    End Function

    Private Sub Initialize()
        objDBLevel_GuiItems_L1 = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_GuiItems_L2 = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_GuiCaption = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_ToolTips = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_Messages = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_LocalizedMessages = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_Languages = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_LanguageShort = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_Caption = New OntologyModDBConnector(localConfig.Globals)
        objDBLevel_Message = New OntologyModDBConnector(localConfig.Globals)

        If GetData_001_GuiEntries().GUID = localConfig.Globals.LState_Success.GUID Then
            If GetData_002_GuiCaptions().GUID = localConfig.Globals.LState_Success.GUID Then
                If GetData_003_ToolTipMessages().GUID = localConfig.Globals.LState_Success.GUID Then
                    If GetData_004_Messages().GUID = localConfig.Globals.LState_Success.GUID Then
                        If GetData_005_LocalizedMessage().GUID = localConfig.Globals.LState_Success.GUID Then
                            If GetData_006_Languages().GUID = localConfig.Globals.LState_Success.GUID Then
                                If GetData_007_LanguageShort().GUID = localConfig.Globals.LState_Success.GUID Then
                                    If GetData_008_CaptionMessage().GUID = localConfig.Globals.LState_Success.GUID Then

                                    Else
                                        Err.Raise(1, "Data-Error!")
                                    End If
                                Else
                                    Err.Raise(1, "Data-Error!")
                                End If
                            Else
                                Err.Raise(1, "Data-Error!")
                            End If
                        Else
                            Err.Raise(1, "Data-Error!")
                        End If
                    Else
                        Err.Raise(1, "Data-Error!")
                    End If
                Else
                    Err.Raise(1, "Data-Error!")
                End If
            Else
                Err.Raise(1, "Data-Error!")
            End If
        Else
            Err.Raise(1, "Data-Error!")
        End If
    End Sub

    Private Function GetData_001_GuiEntries() As clsOntologyItem
        Dim searchGuiEntriesL1 = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Object = localConfig.ID_Development,
                                                                                         .ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                                                                                         .ID_Parent_Other = localConfig.OItem_type_gui_entires.GUID}}

        Dim objOItem_Result = objDBLevel_GuiItems_L1.GetDataObjectRel(searchGuiEntriesL1, doIds:=False)

        If objOItem_Result.GUID = localConfig.Globals.LState_Success.GUID Then
            Dim searchGuiEntriesL2 = objDBLevel_GuiItems_L1.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                         .ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                                                                                                                         .ID_Parent_Other = localConfig.OItem_type_gui_entires.GUID}).ToList()

            If searchGuiEntriesL2.Any() Then
                objOItem_Result = objDBLevel_GuiItems_L2.GetDataObjectRel(searchGuiEntriesL2, doIds:=False)
            End If
        End If

        Return objOItem_Result
    End Function

    Private Function GetData_002_GuiCaptions() As clsOntologyItem
        Dim searchGuiCaptions = objDBLevel_GuiItems_L1.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                    .ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                                                                                                                    .ID_Parent_Other = localConfig.OItem_type_gui_caption.GUID}).ToList()

        searchGuiCaptions.AddRange(objDBLevel_GuiItems_L2.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                       .ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                                                                                                                       .ID_Parent_Other = localConfig.OItem_type_gui_caption.GUID}))

        Dim objOItem_Result = localConfig.Globals.LState_Success.Clone()

        If searchGuiCaptions.Any() Then
            objOItem_Result = objDBLevel_GuiCaption.GetDataObjectRel(searchGuiCaptions, doIds:=False)
        End If

        Return objOItem_Result
    End Function

    Private Function GetData_003_ToolTipMessages() As clsOntologyItem
        Dim searchToolTips = objDBLevel_GuiItems_L1.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                 .ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                                                                                                                 .ID_Parent_Other = localConfig.OItem_type_tooltip_messages.GUID}).ToList()

        searchToolTips.AddRange(objDBLevel_GuiItems_L2.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                 .ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                                                                                                                 .ID_Parent_Other = localConfig.OItem_type_tooltip_messages.GUID}))

        Dim objOItem_Result = localConfig.Globals.LState_Success.Clone()

        If searchToolTips.Any() Then
            objOItem_Result = objDBLevel_ToolTips.GetDataObjectRel(searchToolTips, doIds:=False)
        End If

        Return objOItem_Result
    End Function

    Private Function GetData_004_Messages() As clsOntologyItem
        Dim searchMessages = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Object = localConfig.ID_Development,
                                                                                     .ID_RelationType = localConfig.OItem_relationtype_user_message.GUID,
                                                                                     .ID_Parent_Other = localConfig.OItem_class_messages.GUID},
                                                             New clsObjectRel With {.ID_Object = localConfig.ID_Development,
                                                                                     .ID_RelationType = localConfig.OItem_relationtype_inputmessage.GUID,
                                                                                     .ID_Parent_Other = localConfig.OItem_class_messages.GUID},
                                                             New clsObjectRel With {.ID_Object = localConfig.ID_Development,
                                                                                     .ID_RelationType = localConfig.OItem_relationtype_errormessage.GUID,
                                                                                     .ID_Parent_Other = localConfig.OItem_class_messages.GUID}}

        Dim objOItem_Result = objDBLevel_Messages.GetDataObjectRel(searchMessages, doIds:=False)

        Return objOItem_Result
    End Function

    Private Function GetData_005_LocalizedMessage() As clsOntologyItem
        Dim searchLocalizedMessages = objDBLevel_Messages.ObjectRels.Select(Function(msg) New clsObjectRel With {.ID_Object = msg.ID_Other,
                                                                                                                      .ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                                                                                                                      .ID_Parent_Other = localConfig.OItem_class_localized_message.GUID}).ToList()

        Dim objOItem_Result = localConfig.Globals.LState_Success.Clone()

        If searchLocalizedMessages.Any() Then
            objOItem_Result = objDBLevel_LocalizedMessages.GetDataObjectRel(searchLocalizedMessages, doIds:=False)
        End If

        Return objOItem_Result
    End Function

    Private Function GetData_006_Languages() As clsOntologyItem
        Dim searchLanguages = objDBLevel_GuiCaption.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                  .ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                                                                                                                  .ID_Parent_Other = localConfig.OItem_type_language.GUID}).ToList()

        searchLanguages.AddRange(objDBLevel_ToolTips.ObjectRels.Select(Function(guie) New clsObjectRel With {.ID_Object = guie.ID_Other,
                                                                                                                  .ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                                                                                                                  .ID_Parent_Other = localConfig.OItem_type_language.GUID}))

        searchLanguages.AddRange(objDBLevel_LocalizedMessages.ObjectRels.Select(Function(lmsg) New clsObjectRel With {.ID_Object = lmsg.ID_Other,
                                                                                                                           .ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                                                                                                                           .ID_Parent_Other = localConfig.OItem_type_language.GUID}))

        Dim objOItem_Result = localConfig.Globals.LState_Success.Clone()

        If searchLanguages.Any() Then
            objOItem_Result = objDBLevel_Languages.GetDataObjectRel(searchLanguages, doIds:=False)
        End If

        Return objOItem_Result
    End Function

    Private Function GetData_007_LanguageShort() As clsOntologyItem
        Dim searchLanguageShort = objDBLevel_Languages.ObjectRels.Select(Function(lang) New clsObjectAtt With {.ID_Object = lang.ID_Other,
                                                                                                                    .ID_AttributeType = localConfig.OItem_attributetype_short.GUID}).ToList()

        Dim objOItem_Result = localConfig.Globals.LState_Success.Clone()

        If searchLanguageShort.Any() Then
            objOItem_Result = objDBLevel_LanguageShort.GetDataObjectAtt(searchLanguageShort, doIds:=False)

        End If

        Return objOItem_Result
    End Function

    Private Function GetData_008_CaptionMessage() As clsOntologyItem
        Dim searchCaption = objDBLevel_LocalizedMessages.ObjectRels.Select(Function(lmsg) New clsObjectAtt With {.ID_Object = lmsg.ID_Other,
                                                                                                                      .ID_AttributeType = localConfig.OItem_attribute_caption.GUID}).ToList()

        Dim objOItem_Result = localConfig.Globals.LState_Success.Clone()

        If searchCaption.Any() Then
            objOItem_Result = objDBLevel_Caption.GetDataObjectAtt(searchCaption, doIds:=False)

        End If

        If objOItem_Result.GUID = localConfig.Globals.LState_Success.GUID Then
            Dim searchMessage = objDBLevel_LocalizedMessages.ObjectRels.Select(Function(lmsg) New clsObjectAtt With {.ID_Object = lmsg.ID_Other,
                                                                                                                          .ID_AttributeType = localConfig.OItem_attributetype_message.GUID}).ToList()

            If searchMessage.Any() Then
                objOItem_Result = objDBLevel_Message.GetDataObjectAtt(searchMessage, doIds:=False)
            End If
        End If

        Return objOItem_Result
    End Function
End Class
