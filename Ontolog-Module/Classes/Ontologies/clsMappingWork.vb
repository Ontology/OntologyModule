﻿Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector
Public Class clsMappingWork
    Private objGlobals As Globals
    Private objDBLevel_OntologyMappingItems As OntologyModDBConnector
    Private objDBLevel_OntologyMapping_To_Rel As OntologyModDBConnector
    Private objDBLevel_OntologyMappingItemTree As OntologyModDBConnector
    Private objDBLevel_OntologyMappingItem_To_Rel As OntologyModDBConnector
    Private objDBLevel_OntologyMappingItem_To_Att As OntologyModDBConnector
    Private objDataWork_Ontologies As clsDataWork_Ontologies
    Private objOItem_Mapping As clsOntologyItem
    Private objMappingItems As List(Of clsMappingItem)

    Private objOItem_Result_MappingItems As clsOntologyItem

    Public ReadOnly Property OItem_Result_MappingItems As clsOntologyItem
        Get
            Return objOItem_Result_MappingItems
        End Get
    End Property


    Public function GetData_Mappings(OItem_Mapping As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem

        Dim objOList_Mappings_To_Rel = New List(Of clsObjectRel) From
        {
            New clsObjectRel With {.ID_Object = OItem_Mapping.GUID, _
                                   .ID_RelationType = objGlobals.RelationType_Apply.GUID, _
                                   .ID_Parent_Other = objGlobals.Class_MappingRule.GUID}, _
            New clsObjectRel With {.ID_Object = OItem_Mapping.GUID, _
                                   .ID_RelationType = objGlobals.RelationType_Src.GUID, _
                                   .ID_Parent_Other = objGlobals.Class_OntologyMappingItem.GUID}, _
            New clsObjectRel With {.ID_Object = OItem_Mapping.GUID, _
                                   .ID_RelationType = objGlobals.RelationType_Dst.GUID, _
                                   .ID_Parent_Other = objGlobals.Class_OntologyMappingItem.GUID}
        }

        

        GetData_MappingItems()
        objOItem_Result = objOItem_Result_MappingItems
        If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
            objOItem_Result = objDBLevel_OntologyMapping_To_Rel.GetDataObjectRel(objOList_Mappings_To_Rel, doIds:=False)

            If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
                objDataWork_Ontologies.GetData_001_OntologyJoinsOfOntologies()
                If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
                    objDataWork_Ontologies.GetData_002_OntologyItemsOfJoins()
                    If objDataWork_Ontologies.OItem_Result_OntologyItemsOfJoins.GUID = objGlobals.LState_Success.GUID Then
                        objDataWork_Ontologies.GetData_RefsOfOntologyItems()
                        If objDataWork_Ontologies.OItem_Result_RefsOfOntologyItems.GUID = objGlobals.LState_Success.GUID Then
                            objOItem_Result = objGlobals.LState_Success


                        Else
                            objOItem_Result = objGlobals.LState_Error
                        End If
                    Else
                        objOItem_Result = objGlobals.LState_Error
                    End If

                End If




            End If
        End If


        Return objOItem_Result
    End Function

    Public Function MapItems(OItem_Mapping As clsOntologyItem) As clsOntologyItem


        Dim objOItem_Result = GetData_Mappings(OItem_Mapping)
        If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
            objMappingItems = New List(Of clsMappingItem)
            objMappingItems = (From objMappingItem In objDBLevel_OntologyMappingItems.Objects1
                                                      Group Join objNavigation In objDBLevel_OntologyMappingItem_To_Att.ObjAtts.Where(Function(p) p.ID_AttributeType = objGlobals.AttributeType_Navigation.GUID).ToList() On objNavigation.ID_Object Equals objMappingItem.GUID Into objNavigations = Group
                                                      From objNavigation In objNavigations.DefaultIfEmpty()
                                                      Group Join objOrderID In objDBLevel_OntologyMappingItem_To_Att.ObjAtts.Where(Function(p) p.ID_AttributeType = objGlobals.AttributeType_OrderID.GUID).ToList() On objOrderID.ID_Object Equals objMappingItem.GUID Into objOrderIDs = Group
                                                      From objOrderID In objOrderIDs.DefaultIfEmpty()
                                                      Join objDirection In objDBLevel_OntologyMappingItem_To_Rel.ObjectRels.Where(Function(p) p.ID_Parent_Other = objGlobals.Class_Directions.GUID).ToList() On objDirection.ID_Object Equals objMappingItem.GUID
                                                      Join objOntologyJoin In objDBLevel_OntologyMappingItem_To_Rel.ObjectRels.Where(Function(p) p.ID_Parent_Other = objGlobals.Class_OntologyJoin.GUID).ToList() On objOntologyJoin.ID_Object Equals objMappingItem.GUID
                                                      Select New clsMappingItem With
                                                             {
                                                                 .ID_MappingItem = objMappingItem.GUID, _
                                                                 .Name_MappingItem = objMappingItem.Name, _
                                                                 .ID_Attribute_Navigation = If(objNavigation Is Nothing, Nothing, objNavigation.ID_Attribute), _
                                                                 .Navigation = If(objNavigation Is Nothing, False, objNavigation.Val_Bit), _
                                                                 .ID_Attribute_OrderID = If(objOrderID Is Nothing, Nothing, objOrderID.ID_Attribute), _
                                                                 .OrderID = If(objOrderID Is Nothing, 0, objOrderID.Val_Lng), _
                                                                 .ID_Direction = objDirection.ID_Other, _
                                                                 .Name_Direction = objDirection.Name_Other, _
                                                                 .ID_OntologyJoin = objOntologyJoin.ID_Other, _
                                                                 .Name_OntologyJoin = objOntologyJoin.Name_Other
                                                             }).ToList()

            Dim list_MappingItem_Src = (From objMapping In objDBLevel_OntologyMapping_To_Rel.ObjectRels.Where(Function(p) p.ID_RelationType = objGlobals.RelationType_Src.GUID).ToList()
                                           Join objMappingItem In objMappingItems On objMappingItem.ID_MappingItem Equals objMapping.ID_Other
                                           Select objMappingItem).ToList()

            Dim list_MappingItem_Dst = (From objMapping In objDBLevel_OntologyMapping_To_Rel.ObjectRels.Where(Function(p) p.ID_RelationType = objGlobals.RelationType_Dst.GUID).ToList()
                                           Join objMappingItem In objMappingItems On objMappingItem.ID_MappingItem Equals objMapping.ID_Other
                                           Select objMappingItem).ToList()

            If list_MappingItem_Src.Count = 1 And list_MappingItem_Dst.Any() Then

            Else
                objOItem_Result = objGlobals.LState_Error.Clone()
            End If

        End If
        
        Return objOItem_Result
    End Function

    Public Sub GetData_MappingItems() 
        Dim objOList_MappingItem_To_Rel = New List(Of clsObjectRel) From
        {
            New clsObjectRel With {.ID_Parent_Object = objGlobals.Class_OntologyMappingItem.GUID, _
                                   .ID_RelationType = objGlobals.RelationType_belongsTo.GUID, _
                                   .ID_Parent_Other = objGlobals.Class_OntologyJoin.GUID}, _
            New clsObjectRel With {.ID_Parent_Object = objGlobals.Class_OntologyMappingItem.GUID, _
                                   .ID_RelationType = objGlobals.RelationType_belonging.GUID, _
                                   .ID_Parent_Other = objGlobals.Class_Directions.GUID}
        }

        Dim objOList_MappingItem_To_Att = New List(Of clsObjectAtt) From
        {
            New clsObjectAtt With {.ID_Class = objGlobals.Class_OntologyMappingItem.GUID, _
                                   .ID_AttributeType = objGlobals.AttributeType_Navigation.GUID}, _
            New clsObjectAtt With {.ID_Class = objGlobals.Class_OntologyMappingItem.GUID, _
                                   .ID_AttributeType = objGlobals.AttributeType_OrderID.GUID}
        }

        Dim objOList_Objects = New List(Of clsOntologyItem) From {New clsOntologyItem With {.GUID_Parent = objGlobals.Class_OntologyMappingItem.GUID}}


        objOItem_Result_MappingItems = objGlobals.LState_Nothing.Clone()
        Dim objOItem_Result = objDBLevel_OntologyMappingItems.GetDataObjects(objOList_Objects)
        If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
            objOItem_Result = objDBLevel_OntologyMappingItemTree.GetDataObjectsTree(objGlobals.Class_OntologyMappingItem, objGlobals.Class_OntologyMappingItem, objGlobals.RelationType_contains)
            If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
                objOItem_Result = objDBLevel_OntologyMappingItem_To_Rel.GetDataObjectRel(objOList_MappingItem_To_Rel, doIds:=False)
                If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then
                    objOItem_Result = objDBLevel_OntologyMappingItem_To_Att.GetDataObjectAtt(objOList_MappingItem_To_Att, doIds:=False)
                    If objOItem_Result.GUID = objGlobals.LState_Success.GUID Then

                    End If
                End If
            End If
        End If
        
        

        objOItem_Result_MappingItems = objOItem_Result
        
    End Sub

    Public Sub New(objGlobals As Globals)
        Me.objGlobals = objGlobals
        Initialize()
    End Sub

    Private sub Initialize()
        objDBLevel_OntologyMapping_To_Rel = New OntologyModDBConnector(objGlobals)
        objDBLevel_OntologyMappingItemTree = New OntologyModDBConnector(objGlobals)
        objDBLevel_OntologyMappingItem_To_Rel = New OntologyModDBConnector(objGlobals)
        objDBLevel_OntologyMappingItem_To_Att = New OntologyModDBConnector(objGlobals)
        objDBLevel_OntologyMappingItems = New OntologyModDBConnector(objGlobals)

        objDataWork_Ontologies = New clsDataWork_Ontologies(objGlobals)

        objOItem_Result_MappingItems = objGlobals.LState_Nothing.Clone()
    End Sub
End Class
