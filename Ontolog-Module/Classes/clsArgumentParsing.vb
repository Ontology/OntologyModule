﻿Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector

Public Class clsArgumentParsing

    Public ReadOnly Property Session As String
        Get
            Return ArgumentParsing.Session
        End Get
    End Property

    Public ReadOnly Property OList_Items As List(Of clsOntologyItem)
        Get
            Return ArgumentParsing.Items
        End Get
    End Property
    Public ReadOnly Property FunctionList As List(Of clsModuleFunction)
        Get
            Dim _functionList = ArgumentParsing.FunctionList.Select(Function(func) New clsModuleFunction With {.GUID_DestOntology = func.GUIDDestOntology,
                                                                                                              .GUID_Function = func.GUIDFunction,
                                                                                                              .Name_Function = func.NameFunction}).ToList()
            Return _functionList
        End Get
    End Property

    Public ReadOnly Property UnparsedArguments As List(Of String)
        Get
            Return ArgumentParsing.UnparsedArguments
        End Get
    End Property

    Public ReadOnly Property External As String
        Get
            Return ArgumentParsing.External
        End Get
    End Property

    Public Sub New(objGlobals As Globals, arguments As List(Of String), Optional boolReparse As Boolean = False)
        If boolReparse = False Then
            ArgumentParsing.ParseArguments(objGlobals, arguments)
        Else
            ArgumentParsing.ReParseArguments(objGlobals, arguments)
        End If

    End Sub


End Class
