﻿Imports System.Reflection
Imports OntologyClasses.BaseClasses
Imports ClassLibrary_ShellWork
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports Structure_Module
Imports System.Runtime.InteropServices

Public Class UserControl_OItemList
    Private objLocalConfig As clsLocalConfig

    Private otblT_Objects As New DataSet_Config.otbl_ObjectsDataTable

    Private objDBLevel As OntologyModDBConnector
    Private objDBLevel2 As OntologyModDBConnector
    Private objDBLevel3 As OntologyModDBConnector
    Private objDBLevel4 As OntologyModDBConnector
    Private objDBLevel5 As OntologyModDBConnector
    Private objOntologyClipboard As clsOntologyClipboard
    Private objSession As clsSession = Nothing

    Private objFrm_Main As frmMain
    Private objFrm_ObjectEdit As frm_ObjectEdit
    Private objFrm_AttributeTypeEdit As frm_AttributeTypeEdit
    Private objFrm_Clipboard As frmClipboard
    Private objFrm_Replace As frmReplace
    Private objFrm_RelationFilter As frmRelationFilter
    Private objFrm_Modules As frmModules
    Private objFrm_Name As frm_Name

    Private objShellWork As clsShellWork

    Private WithEvents objTransaction_Objects As clsTransaction_Objects
    Private objTransaction_RelationTypes As clsTransaction_RelationTypes
    Private objTransaction_AttributeTypes As clsTransaction_AttributeTypes
    Private objTransaction As clsTransaction
    Private objRelationConfig As clsRelationConfig

    Private objOItem_Parent As clsOntologyItem

    Private objOItem_Class As New clsOntologyItem
    Private objOItem_RelationType As clsOntologyItem
    Private objOItem_Other As clsOntologyItem
    Private objOItem_Object As clsOntologyItem

    Public Property OItem_Class_AdvancedFilter As clsOntologyItem
    Public Property OItem_RelationType_AdvancedFilter As clsOntologyItem
    Public Property OItem_Object_AdvancedFilter As clsOntologyItem
    Public Property OItem_Direction_AdvancedFilter As clsOntologyItem


    Private objThread_List As Threading.Thread

    Private boolProgChange As Boolean

    Private strRowName_GUID As String
    Private strRowName_Name As String
    Private strRowName_ParentGUID As String
    Private strGUID_Class As String

    Private objOItem_Direction As clsOntologyItem

    Private oList_Selected_Simple As New List(Of clsOntologyItem)
    Private oList_Selected_ObjectRel As New List(Of clsObjectRel)

    Private objFrmAdvancedFilter As frmAdvancedFilter

    Private objDlg_Attribute_Boolean As dlg_Attribute_Boolean
    Private objDlg_Attribute_DateTime As dlg_Attribute_DateTime
    Private objDlg_Attribute_Long As dlg_Attribute_Long
    Private objDlg_Attribute_Double As dlg_Attribute_Double
    Private objDlg_Attribute_String As dlg_Attribute_String

    Private objFrmOItemListToShow As frmOntologyItemList

    Private boolOR As Boolean

    Private strName_Filter As String
    Private strGUID_Filter As String

    Private intRowID As Integer

    Private boolFinished As Boolean
    Private boolApplyable As Boolean

    Private strType As String

    Private strFilter_Extern As String

    Private strLastModule As String

    Private strGuidApplyModule As String
    Private strNameApplyModule As String
    Private strTextToolstripApplyModule As String

    Private Event selected_ListItem()

    Public Event Selection_Changed()
    Public Event edit_Object(ByVal strType As String, ByVal oItem_Direction As clsOntologyItem)

    Public Event applied_Items()
    Public Event counted_Items(ByVal intCount As Integer)
    Public Event addedHandOffItems(oList_Simple As List(Of clsOntologyItem))
    Public Event addedSimpleItems(olist_Simple As List(Of clsOntologyItem))
    Public Event ListDataFinished()

    Public Property HandOff_Add As Boolean

    Private boolNullRelation As Boolean

    Private objPropertyFilters As List(Of PropertyFilter)
    Private objGridConfigurator As New clsGridConfigurator()

    Private Sub NameError() Handles objTransaction_Objects.ErrorNaming
        MsgBox("Die Objekte verstoßen gegen eine Namenskonvention!", MsgBoxStyle.Information)
    End Sub

    Public ReadOnly Property AdvancedFilterApplied As Boolean
        Get
            Return ToolStripButton_FilterAdvanced.Checked
        End Get
    End Property

    Public ReadOnly Property SelectedRowIndex As Long
        Get
            If DataGridView_Items.SelectedRows.Count > 0 Then
                Return DataGridView_Items.SelectedRows(0).Index
            Else
                Return -1
            End If
        End Get
    End Property

    Public ReadOnly Property ColumnList As DataGridViewColumnCollection
        Get
            Return DataGridView_Items.Columns
        End Get
    End Property

    Public ReadOnly Property RowName_GUID As String
        Get
            Return strRowName_GUID
        End Get
    End Property

    Public ReadOnly Property RowName_Name As String
        Get
            Return strRowName_Name
        End Get
    End Property

    Public ReadOnly Property RowName_GUIDParent As String
        Get
            Return strRowName_ParentGUID
        End Get
    End Property

    Public ReadOnly Property OList_Simple As List(Of clsOntologyItem)
        Get
            Return oList_Selected_Simple
        End Get
    End Property

    Public ReadOnly Property OList_ObjectRel As List(Of clsObjectRel)
        Get
            Return oList_Selected_ObjectRel
        End Get
    End Property

    Public Property Applyable As Boolean
        Get
            Return boolApplyable
        End Get
        Set(ByVal value As Boolean)
            boolApplyable = value
        End Set
    End Property

    Public ReadOnly Property RowID As Integer
        Get
            Return intRowID
        End Get
    End Property

    Public ReadOnly Property DataGridviewRows As DataGridViewRowCollection
        Get
            Return DataGridView_Items.Rows
        End Get
    End Property

    Public ReadOnly Property DataGridViewRowCollection_Selected As DataGridViewSelectedRowCollection
        Get
            Return DataGridView_Items.SelectedRows
        End Get
    End Property

    Public Property VisibilityToolStrip As Boolean
        Get
            Return ToolStrip_Edit.Visible
        End Get
        Set(value As Boolean)
            ToolStrip_Edit.Visible = value
        End Set
    End Property

    Public Sub select_Row(OItem_Item As clsOntologyItem)
        Dim items = DataGridView_Items.DataSource
        If TypeOf (items) Is SortableBindingList(Of clsOntologyItem) Then
            Dim itemList = CType(items, SortableBindingList(Of clsOntologyItem))
            Dim itemFind = itemList.Where(Function(item) item.GUID = OItem_Item.GUID).FirstOrDefault()
            If Not itemFind Is Nothing Then
                Dim ix = itemList.IndexOf(OItem_Item)
                DataGridView_Items.Rows(ix).Selected = True
                DataGridView_Items.FirstDisplayedScrollingRowIndex = ix
            End If

        End If

    End Sub

    Public Sub select_Row(index As Long)

        If index > -1 Then
            DataGridView_Items.Rows(index).Selected = True
            DataGridView_Items.FirstDisplayedScrollingRowIndex = index
        End If


    End Sub

    Private Sub LocalizeGui()
        objLocalConfig.LocalizeGui.ConfigureControlsLanguage(Me, Me.Name)
    End Sub

    Public Sub initialize(ByVal OItem_Parent As clsOntologyItem, Optional ByVal oItem_Object As clsOntologyItem = Nothing, Optional ByVal OItem_Direction As clsOntologyItem = Nothing, Optional ByVal OItem_Other As clsOntologyItem = Nothing, Optional ByVal OItem_RelType As clsOntologyItem = Nothing, Optional ByVal boolOR As Boolean = False, Optional strFilter As String = Nothing)
        boolProgChange = True
        HandOff_Add = False
        Me.boolOR = boolOR
        clear_Relation()
        strGUID_Class = Nothing

        strGUID_Filter = ""
        strName_Filter = ""

        If Not strFilter Is Nothing Then
            If objLocalConfig.Globals.is_GUID(strFilter) Then
                strGUID_Filter = strFilter
            Else
                strName_Filter = strFilter
            End If


        End If

        OItem_Class_AdvancedFilter = Nothing
        OItem_RelationType_AdvancedFilter = Nothing
        OItem_Object_AdvancedFilter = Nothing

        ToolStripTextBox_Filter.ReadOnly = True
        ToolStripTextBox_Filter.Text = ""
        ToolStripButton_Filter.Checked = False
        ToolStripTextBox_Filter.ReadOnly = False
        objPropertyFilters = Nothing

        If OItem_Direction Is Nothing Then
            If Not oItem_Object Is Nothing Then
                If oItem_Object.GUID <> "" Then
                    strGUID_Filter = oItem_Object.GUID
                ElseIf oItem_Object.Name <> "" Then
                    strGUID_Filter = oItem_Object.Name
                End If
            End If
            objOItem_Other = OItem_Other


            objOItem_Object = oItem_Object


            objOItem_RelationType = OItem_RelType
            objOItem_Parent = OItem_Parent


            If objOItem_Parent Is Nothing Then
                strType = OItem_Other.Type
            Else
                strType = objOItem_Parent.Type
            End If



        Else

            strType = objLocalConfig.Globals.Type_Other
            objOItem_Direction = OItem_Direction
            If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                If Not oItem_Object Is Nothing Then
                    If oItem_Object.GUID <> "" Then
                        strGUID_Filter = oItem_Object.GUID
                    ElseIf oItem_Object.Name <> "" Then
                        strGUID_Filter = oItem_Object.Name
                    End If
                End If
                objOItem_Object = oItem_Object
                objOItem_Other = OItem_Other

                objOItem_RelationType = OItem_RelType
                objOItem_Parent = OItem_Parent
            Else
                If Not OItem_Other Is Nothing Then
                    strGUID_Class = OItem_Other.GUID_Parent
                End If
                If Not oItem_Object Is Nothing Then
                    objOItem_Other = oItem_Object

                End If

                objOItem_Parent = OItem_Parent
                objOItem_RelationType = OItem_RelType
            End If

        End If


        ToolStripButton_AddItem.Visible = True
        ToolStripButton_DelItem.Visible = True
        ToolStripButton_Up.Visible = False
        ToolStripButton_Down.Visible = False
        ToolStripButton_Sort.Visible = True
        ToolStripButton_Report.Visible = True
        ToolStripTextBox_Filter.ReadOnly = True
        ToolStripTextBox_Filter.Text = ""
        ToolStripTextBox_Filter.ReadOnly = False

        get_RowNames()
        configure_TabPages()

        boolProgChange = False
    End Sub

    Private Sub get_RowNames()
        Select Case strType
            Case objLocalConfig.Globals.Type_AttributeType
                strRowName_GUID = "ID_Item"
                strRowName_Name = "Name"
                strRowName_ParentGUID = "ID_Parent"
            Case objLocalConfig.Globals.Type_Class
                strRowName_GUID = "ID_Item"
                strRowName_Name = "Name"
                strRowName_ParentGUID = "ID_Parent"
            Case objLocalConfig.Globals.Type_Object
                strRowName_GUID = "ID_Item"
                strRowName_Name = "Name"
                strRowName_ParentGUID = "ID_Parent"
            Case objLocalConfig.Globals.Type_RelationType
                strRowName_GUID = "ID_Item"
                strRowName_Name = "Name"
                strRowName_ParentGUID = ""
            Case objLocalConfig.Globals.Type_Other
                Select Case objOItem_Direction.GUID
                    Case objLocalConfig.Globals.Direction_LeftRight.GUID
                        strRowName_GUID = "ID_Other"
                        strRowName_Name = "Name_Other"
                        strRowName_ParentGUID = "ID_Parent_Other"
                    Case objLocalConfig.Globals.Direction_RightLeft.GUID
                        strRowName_GUID = "ID_Object"
                        strRowName_Name = "Name_Object"
                        strRowName_ParentGUID = "ID_Parent_Object"
                End Select
        End Select
    End Sub

    Private Sub GetData_Objects()
        Dim oList_Items_Objects As New List(Of clsOntologyItem)

        If Not OItem_RelationType_AdvancedFilter Is Nothing Or Not OItem_Class_AdvancedFilter Is Nothing Or Not OItem_Object_AdvancedFilter Is Nothing Then
            Dim objORel_AdvancedFilter = New List(Of clsObjectRel)
            If OItem_Direction_AdvancedFilter.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                objORel_AdvancedFilter = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Parent_Object = strGUID_Class, _
                                                                                                 .ID_RelationType = If(Not OItem_RelationType_AdvancedFilter Is Nothing, OItem_RelationType_AdvancedFilter.GUID, Nothing), _
                                                                                                 .ID_Parent_Other = If(Not OItem_Class_AdvancedFilter Is Nothing And OItem_Object_AdvancedFilter Is Nothing, OItem_Class_AdvancedFilter.GUID, Nothing), _
                                                                                                 .ID_Other = If(Not OItem_Object_AdvancedFilter Is Nothing, OItem_Object_AdvancedFilter.GUID, Nothing)}}
                Dim objOItem_Result = objDBLevel.GetDataObjectRel(objORel_AdvancedFilter, doIds:=False)
                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    If boolNullRelation Then
                        oList_Items_Objects.Add(New clsOntologyItem(strGUID_Filter, strName_Filter, strGUID_Class, objLocalConfig.Globals.Type_Object))
                        objDBLevel2.GetDataObjects(oList_Items_Objects)
                    End If
                End If

            Else
                objORel_AdvancedFilter = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Parent_Other = strGUID_Class, _
                                                                                                 .ID_RelationType = If(Not OItem_RelationType_AdvancedFilter Is Nothing, OItem_RelationType_AdvancedFilter.GUID, Nothing), _
                                                                                                 .ID_Parent_Object = If(Not OItem_Class_AdvancedFilter Is Nothing And OItem_Object_AdvancedFilter Is Nothing, OItem_Class_AdvancedFilter.GUID, Nothing), _
                                                                                                 .ID_Object = If(Not OItem_Object_AdvancedFilter Is Nothing, OItem_Object_AdvancedFilter.GUID, Nothing)}}
                Dim objOItem_Result = objDBLevel.GetDataObjectRel(objORel_AdvancedFilter, doIds:=False)
                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    If boolNullRelation Then
                        oList_Items_Objects.Add(New clsOntologyItem(strGUID_Filter, strName_Filter, strGUID_Class, objLocalConfig.Globals.Type_Object))
                        objDBLevel2.GetDataObjects(oList_Items_Objects)
                    End If
                End If

            End If



        Else
            oList_Items_Objects.Add(New clsOntologyItem(strGUID_Filter, strName_Filter, strGUID_Class, objLocalConfig.Globals.Type_Object))
            objDBLevel.GetDataObjects(oList_Items_Objects)
        End If
    End Sub

    Private Sub GetData_RelationTypes()
        Dim oList_Items As New List(Of clsOntologyItem)

        oList_Items.Add(New clsOntologyItem(strGUID_Filter, strName_Filter, objLocalConfig.Globals.Type_RelationType))
        objDBLevel.GetDataRelationTypes(oList_Items)
    End Sub

    Private Sub GetData_AttributeTypes()
        Dim oList_Items As New List(Of clsOntologyItem)

        oList_Items.Add(New clsOntologyItem(strGUID_Filter, strName_Filter, objLocalConfig.Globals.Type_AttributeType))
        objDBLevel.GetDataAttributeType(oList_Items)
    End Sub

    Private Sub GetData_ObjAtt()
        Dim oList_ObjAtt As New List(Of clsObjectAtt)
        oList_ObjAtt.Add(New clsObjectAtt With {.ID_Object = objOItem_Object.GUID,
                                                .ID_AttributeType = objOItem_Other.GUID})
        objDBLevel.GetDataObjectAtt(oList_ObjAtt)
    End Sub

    Private Sub GetData_ObjRelWithOther()
        Dim oList_ObjRel As New List(Of clsObjectRel)
        oList_ObjRel.Add(New clsObjectRel With {.ID_Object = strGUID_Filter,
                                                .Name_Object = strName_Filter,
                                                .ID_Parent_Object = strGUID_Class,
                                                .ID_Other = objOItem_Other.GUID,
                                                .Name_Other = objOItem_Other.Name,
                                                .ID_Parent_Other = objOItem_Other.GUID_Parent,
                                                .ID_RelationType = objOItem_RelationType.GUID})

        objDBLevel.GetDataObjectRel(oList_ObjRel, False, False)
    End Sub

    Private Sub GetData_ObjRelWithoutOther()
        Dim oList_ObjRel As New List(Of clsObjectRel)
        If Not objOItem_Object Is Nothing Then
            If Not objOItem_Object.GUID_Parent Is Nothing Then
                strGUID_Class = objOItem_Object.GUID_Parent
            End If
        End If

        oList_ObjRel.Add(New clsObjectRel With {.ID_Object = strGUID_Filter,
                                                .Name_Object = strName_Filter,
                                                .ID_Parent_Object = strGUID_Class,
                                                .ID_RelationType = objOItem_RelationType.GUID,
                                                .ID_Direction = objLocalConfig.Globals.Direction_LeftRight.GUID})

        objDBLevel.GetDataObjectRel(oList_ObjRel, False, False)
    End Sub

    Private Sub get_Data()



        Dim objOItem_Item As clsOntologyItem



        If Not objOItem_Parent Is Nothing Then
            If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                strGUID_Class = objOItem_Parent.GUID_Parent
            Else
                strGUID_Class = objOItem_Parent.GUID
            End If
            If boolOR = False Then
                Select Case objOItem_Parent.Type
                    Case objLocalConfig.Globals.Type_Object
                        GetData_Objects()

                    Case objLocalConfig.Globals.Type_RelationType
                        GetData_RelationTypes()

                    Case objLocalConfig.Globals.Type_AttributeType
                        GetData_AttributeTypes()

                End Select
            Else

            End If


        Else
            If Not objOItem_Other Is Nothing Then
                Select Case objOItem_Other.Type



                    Case objLocalConfig.Globals.Type_RelationType
                        Err.Raise(1, "Not Implemented")

                    Case objLocalConfig.Globals.Type_AttributeType

                        GetData_ObjAtt()

                    Case Else

                        GetData_ObjRelWithOther()
                End Select
            Else
                GetData_ObjRelWithoutOther()


            End If


        End If
        boolFinished = True
    End Sub

    Private Sub configure_TabPages()
        ToolStripButton_FilterAdvanced.Enabled = False
        Timer_List.Stop()
        Timer_Filter.Stop()
        Select Case TabControl1.SelectedTab.Name
            Case TabPage_List.Name
                If Not objThread_List Is Nothing Then
                    Try
                        objThread_List.Abort()
                    Catch ex As Exception

                    End Try
                End If


                DataGridView_Items.DataSource = Nothing

                boolFinished = False
                objThread_List = New Threading.Thread(AddressOf get_Data)


                objThread_List.Start()
                Timer_List.Start()

            Case TabPage_Tree.Name
                If Not objOItem_Parent Is Nothing Then
                    'objUserControl_TreeOfToken.initialize(objSemItem_Parent)
                End If
        End Select

    End Sub

    Public Sub clear_Relation()
        Try
            objThread_List.Abort()
            Timer_Filter.Stop()
            Timer_List.Stop()
        Catch ex As Exception

        End Try
        objOItem_Other = Nothing
        objOItem_RelationType = Nothing
        objOItem_Direction = Nothing
        strGUID_Filter = Nothing
        strName_Filter = Nothing
        ToolStripButton_Relate.Checked = False
        ToolStripButton_Relate.Enabled = False
        ToolStripTextBox_Filter.ReadOnly = True
        ToolStripTextBox_Filter.Clear()
        ToolStripTextBox_Filter.ReadOnly = False

        DataGridView_Items.DataSource = Nothing

    End Sub


    Public Sub New(ByVal objGlobals As Globals)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(objGlobals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        set_DBConnection()
    End Sub

    Public Sub New(ByVal LocalConfig As clsLocalConfig)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfig
        set_DBConnection()
    End Sub

    Private Sub set_DBConnection()
        strTextToolstripApplyModule = ToolStripMenuItem_ChooseModule.Text

        objDBLevel = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel2 = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel3 = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel4 = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel5 = New OntologyModDBConnector(objLocalConfig.Globals)

        objTransaction_Objects = New clsTransaction_Objects(objLocalConfig, Me)
        objTransaction_AttributeTypes = New clsTransaction_AttributeTypes(objLocalConfig, Me)
        objTransaction_RelationTypes = New clsTransaction_RelationTypes(objLocalConfig, Me)
        objTransaction = New clsTransaction(objLocalConfig.Globals)
        objOntologyClipboard = New clsOntologyClipboard(objLocalConfig)
        objRelationConfig = New clsRelationConfig(objLocalConfig.Globals)

        LocalizeGui()

        'If ToolStripComboBox_ModuleList.Items.Count = 0 Then
        '    Dim objResult = objDBLevel.GetDataObjects(New List(Of clsOntologyItem) From {New clsOntologyItem With {.GUID_Parent = objLocalConfig.Globals.Class_Module.GUID}})
        '    If objResult.GUID = objLocalConfig.Globals.LState_Success.GUID Then
        '        Dim objModuleList = objDBLevel.Objects1.GroupBy(Function(objMod) New With {.GUID = objMod.GUID, .Name = objMod.Name, .GUID_Parent = objMod.GUID_Parent}) _
        '            .Select(Function(objMod) New clsOntologyItem With {.GUID = objMod.Key.GUID, .Name = objMod.Key.Name, .GUID_Parent = objMod.Key.GUID_Parent, .Type = objLocalConfig.Globals.Type_Object})

        '        ToolStripComboBox_ModuleList.Items.AddRange(objModuleList)
        '        ToolStripComboBox_ModuleList.ComboBox.ValueMember = "GUID"
        '        ToolStripComboBox_ModuleList.ComboBox.DisplayMember = "Name"
        '    Else
        '        MsgBox("Die Module konnten nicht ermittelt werden!", MsgBoxStyle.Exclamation)
        '    End If
        'End If

    End Sub

    Private Sub DataGridView_Items_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView_Items.CellDoubleClick
        Dim objDGVR_Selected As DataGridViewRow
        Dim objOList_Relation As New List(Of clsObjectRel)
        Dim objOLIst_Att As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem
        Dim objColumn As DataGridViewTextBoxColumn
        Dim boolRel As Boolean = False
        Dim boolChange As Boolean = False

        If Not e.ColumnIndex = -1 Then
            If DataGridView_Items.Columns(e.ColumnIndex).DataPropertyName.ToLower = "orderid" Then
                objDGVR_Selected = DataGridView_Items.Rows(e.RowIndex)
                Dim objItem = objDGVR_Selected.DataBoundItem
                If TypeOf (objItem) Is clsObjectRel Then
                    Dim objRel = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)

                    objDlg_Attribute_Long = New dlg_Attribute_Long("New OrderID", objLocalConfig, objRel.OrderID)
                    objDlg_Attribute_Long.ShowDialog(Me)

                    If (objDlg_Attribute_Long.DialogResult = DialogResult.OK) Then
                        objRel.OrderID = objDlg_Attribute_Long.Value
                        objOList_Relation.Add(objRel)

                        objOItem_Result = objDBLevel.SaveObjRel(objOList_Relation)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Die Gewichtung konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                        End If

                        get_Data()
                    End If
                ElseIf TypeOf (objItem) Is clsObjectAtt Then
                    Dim objRel = CType(objDGVR_Selected.DataBoundItem, clsObjectAtt)

                    objDlg_Attribute_Long = New dlg_Attribute_Long("New OrderID", objLocalConfig, objRel.OrderID)
                    objDlg_Attribute_Long.ShowDialog(Me)
                    If objDlg_Attribute_Long.DialogResult = DialogResult.OK Then
                        objRel.OrderID = objDlg_Attribute_Long.Value
                        objOLIst_Att.Add(objRel)

                        objOItem_Result = objDBLevel.SaveObjAtt(objOLIst_Att)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Die Gewichtung konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                        End If

                        get_Data()

                    End If
                End If

            ElseIf DataGridView_Items.Columns(e.ColumnIndex).DataPropertyName.ToLower = "name" Then
                objDGVR_Selected = DataGridView_Items.Rows(e.RowIndex)
                Dim objItem = objDGVR_Selected.DataBoundItem
                If TypeOf (objItem) Is clsOntologyItem Then
                    Dim objOItem = CType(objItem, clsOntologyItem)
                    If objOItem.Type = objLocalConfig.Globals.Type_RelationType Then
                        objFrm_Name = New frm_Name("Geben Sie bitte einen neuen Namen für den Beziehungstyp ein.", objLocalConfig.Globals, Value1:=objOItem.Name)
                        objFrm_Name.ShowDialog(Me)
                        If objFrm_Name.DialogResult = DialogResult.OK Then
                            If Not String.IsNullOrEmpty(objFrm_Name.Value1) Then
                                Dim objOItemSaveTest = objOItem.Clone()
                                objOItemSaveTest.Name = objFrm_Name.Value1

                                objTransaction.ClearItems()
                                objOItem_Result = objTransaction.do_Transaction(objOItemSaveTest)
                                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                    objOItem.Name = objFrm_Name.Value1
                                ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Relation.GUID Then
                                    MsgBox("Es existiert bereits ein Beziehungstyp mit dem Namen.", MsgBoxStyle.Information)
                                ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                    MsgBox("Beim Speichern des Beziehungstypes ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                                End If
                            Else
                                MsgBox("Geben Sie bitte einen gültigen Namen ein!", MsgBoxStyle.Information)
                            End If

                        End If

                    ElseIf objOItem.Type = objLocalConfig.Globals.Type_Object Then
                        objFrm_Name = New frm_Name("Geben Sie bitte einen neuen Namen für das Objekt ein.", objLocalConfig.Globals, Value1:=objOItem.Name)
                        objFrm_Name.ShowDialog(Me)
                        If objFrm_Name.DialogResult = DialogResult.OK Then

                            Dim objOItem_Object = objOItem.Clone()
                            objOItem_Object.Name = objFrm_Name.Value1

                            objTransaction.ClearItems()
                            objOItem_Result = objTransaction.do_Transaction(objOItem_Object)
                            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                objOItem.Name = objOItem_Object.Name
                            ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                MsgBox("Beim Speichern des Objekts ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                            End If

                        End If
                    End If
                End If

            End If
        End If

    End Sub

    Private Sub DataGridView_Items_KeyDown(sender As Object, e As KeyEventArgs) Handles DataGridView_Items.KeyDown
        If e.KeyCode = Keys.F5 Then
            get_Data()
            Timer_List.Start()
        End If
    End Sub

    Private Sub DataGridView_Items_RowHeaderMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DataGridView_Items.RowHeaderMouseDoubleClick



        Dim objDGVR_Selected As DataGridViewRow
        Dim objOLObjects As New List(Of clsOntologyItem)
        Dim objOAItem_Value As New clsObjectAtt
        Dim objOItem_Result As clsOntologyItem

        objDGVR_Selected = DataGridView_Items.Rows(e.RowIndex)
        Dim objItem = objDGVR_Selected.DataBoundItem

        If TypeOf (objItem) Is clsOntologyItem Then
            Dim objOItem = CType(objItem, clsOntologyItem)

            Select Case objOItem.Type
                Case objLocalConfig.Globals.Type_AttributeType
                    objFrm_AttributeTypeEdit = New frm_AttributeTypeEdit(objLocalConfig, objOItem)
                    objFrm_AttributeTypeEdit.ShowDialog(Me)
                    MsgBox("Implement: AttributeTye-Edit")
                Case objLocalConfig.Globals.Type_Class
                    MsgBox("Implement: Class-Edit")
                Case objLocalConfig.Globals.Type_RelationType
                    MsgBox("Implement: RelationType-Edit")
                Case objLocalConfig.Globals.Type_Object
                    intRowID = objDGVR_Selected.Index

                    'objOLObjects.Add(objOItem_Selected)
                    If (objFrm_ObjectEdit Is Nothing) Then
                        objFrm_ObjectEdit = New frm_ObjectEdit(objLocalConfig, _
                                                               DataGridView_Items.Rows, _
                                                               intRowID, _
                                                               objLocalConfig.Globals.Type_Object, _
                                                               Nothing)
                    Else
                        objFrm_ObjectEdit.RefreshForm(DataGridView_Items.Rows, _
                                                               intRowID, _
                                                               objLocalConfig.Globals.Type_Object, _
                                                               Nothing)
                    End If

                    objFrm_ObjectEdit.ShowDialog(Me)
                    get_Data()
                    Timer_List.Start()
            End Select
        ElseIf TypeOf (objItem) Is clsObjectRel Then
            Dim objORel = CType(objItem, clsObjectRel)
            Select Case objORel.Ontology
                Case objLocalConfig.Globals.Type_Object
                    If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                        objOLObjects.Add(New clsOntologyItem(objORel.ID_Other, _
                                                         objORel.Name_Other, _
                                                         objORel.ID_Parent_Other, _
                                                         objORel.Ontology))
                    Else
                        objOLObjects.Add(New clsOntologyItem(objORel.ID_Object, _
                                                         objORel.Name_Object, _
                                                         objORel.ID_Parent_Object, _
                                                         objLocalConfig.Globals.Type_Object))
                    End If


                    If objFrm_ObjectEdit Is Nothing Then
                        objFrm_ObjectEdit = New frm_ObjectEdit(objLocalConfig, _
                                                   objOLObjects, _
                                                   0, _
                                                   objLocalConfig.Globals.Type_Object, _
                                                   Nothing)
                    Else
                        objFrm_ObjectEdit.RefreshForm(objOLObjects, _
                                                   0, _
                                                   objLocalConfig.Globals.Type_Object, _
                                                   Nothing)
                    End If

                    objFrm_ObjectEdit.ShowDialog(Me)
                    If objFrm_ObjectEdit.DialogResult = DialogResult.OK Then

                    End If
            End Select
        ElseIf TypeOf (objItem) Is clsObjectAtt Then
            Dim objOAItem = CType(objItem, clsObjectAtt)
            Select Case objOAItem.ID_DataType
                Case objLocalConfig.Globals.DType_Bool.GUID
                    objDlg_Attribute_Boolean = New dlg_Attribute_Boolean(objOAItem.Name_Object + ": " + objOAItem.Name_DataType, objLocalConfig.Globals, objOAItem.Val_Bit)
                    objDlg_Attribute_Boolean.ShowDialog(Me)

                    If objDlg_Attribute_Boolean.DialogResult = DialogResult.OK Then
                        objOAItem.Val_Bool = objDlg_Attribute_Boolean.Value

                        objTransaction.ClearItems()

                        objOItem_Result = objTransaction.do_Transaction(objOAItem)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Das Attribute konnte nicht gesetzt werden!", MsgBoxStyle.Exclamation)

                        Else
                            get_Data()
                        End If
                    End If
                Case objLocalConfig.Globals.DType_DateTime.GUID
                    objDlg_Attribute_DateTime = New dlg_Attribute_DateTime(objOAItem.Name_Object + ": " + objOAItem.Name_DataType, objLocalConfig.Globals, objOAItem.Val_Datetime)
                    objDlg_Attribute_DateTime.ShowDialog(Me)

                    If objDlg_Attribute_DateTime.DialogResult = DialogResult.OK Then
                        objOAItem.Val_Datetime = objDlg_Attribute_DateTime.Value

                        objTransaction.ClearItems()

                        objOItem_Result = objTransaction.do_Transaction(objOAItem)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Das Attribute konnte nicht gesetzt werden!", MsgBoxStyle.Exclamation)

                        Else
                            get_Data()
                        End If
                    End If
                Case objLocalConfig.Globals.DType_Int.GUID
                    objDlg_Attribute_Long = New dlg_Attribute_Long(objOAItem.Name_Object + ": " + objOAItem.Name_DataType, objLocalConfig.Globals, objOAItem.Val_Int)
                    objDlg_Attribute_Long.ShowDialog(Me)

                    If objDlg_Attribute_Long.DialogResult = DialogResult.OK Then
                        objOAItem.Val_Int = objDlg_Attribute_Long.Value

                        objTransaction.ClearItems()

                        objOItem_Result = objTransaction.do_Transaction(objOAItem)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Das Attribute konnte nicht gesetzt werden!", MsgBoxStyle.Exclamation)

                        Else
                            get_Data()
                        End If
                    End If
                Case objLocalConfig.Globals.DType_Real.GUID
                    objDlg_Attribute_Double = New dlg_Attribute_Double(objOAItem.Name_Object + ": " + objOAItem.Name_DataType, objLocalConfig.Globals, objOAItem.Val_Real)
                    objDlg_Attribute_Double.ShowDialog(Me)

                    If objDlg_Attribute_Double.DialogResult = DialogResult.OK Then
                        objOAItem.Val_Double = objDlg_Attribute_Double.Value

                        objTransaction.ClearItems()

                        objOItem_Result = objTransaction.do_Transaction(objOAItem)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Das Attribute konnte nicht gesetzt werden!", MsgBoxStyle.Exclamation)

                        Else
                            get_Data()
                        End If
                    End If
                Case objLocalConfig.Globals.DType_String.GUID
                    objDlg_Attribute_String = New dlg_Attribute_String(objOAItem.Name_Object + ": " + objOAItem.Name_DataType, objLocalConfig.Globals, objOAItem.Val_String)
                    objDlg_Attribute_String.ShowDialog(Me)

                    If objDlg_Attribute_String.DialogResult = DialogResult.OK Then
                        objOAItem.Val_String = objDlg_Attribute_String.Value

                        objTransaction.ClearItems()

                        objOItem_Result = objTransaction.do_Transaction(objOAItem)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Das Attribute konnte nicht gesetzt werden!", MsgBoxStyle.Exclamation)

                        Else
                            get_Data()
                        End If
                    End If
            End Select
        End If


    End Sub

    Private Sub DataGridView_Items_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView_Items.SelectionChanged
        Dim objDGVR As DataGridViewRow

        ToolStripButton_Replace.Enabled = False
        If DataGridView_Items.SelectedRows.Count = 1 Then

            objDGVR = DataGridView_Items.SelectedRows(0)
            Dim objItem = objDGVR.DataBoundItem
            If (TypeOf (objItem) Is clsOntologyItem) Then
                Dim objOItem = CType(objItem, clsOntologyItem)

                If Not objLocalConfig.ModuleCommuncation Is Nothing Then
                    objLocalConfig.ModuleCommuncation.SelectedItem(objOItem)
                End If
                Dim objProperty = objOItem.GetType().GetProperty(strRowName_GUID)
                Dim value = objProperty.GetValue(objItem)
                ToolStripTextBox_GUID.Text = value.ToString()
            End If


        Else
            ToolStripTextBox_GUID.Clear()
        End If

        If DataGridView_Items.SelectedRows.Count > 0 And strType = objLocalConfig.Globals.Type_Object Then
            ToolStripButton_Replace.Enabled = True
        End If
        If boolProgChange = False Then
            RaiseEvent Selection_Changed()
        End If
    End Sub

    Private Sub ToolStripTextBox_Filter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripTextBox_Filter.TextChanged
        If ToolStripTextBox_Filter.ReadOnly = False Then
            Timer_List.Stop()
            Timer_Filter.Stop()
            ToolStripButton_Filter.Checked = True
            Timer_Filter.Start()
        End If
    End Sub

    Private Sub Timer_Filter_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_Filter.Tick
        Timer_Filter.Stop()
        Timer_List.Stop()
        Filter_List()
    End Sub

    Public Sub Filter_Items(ByVal strFilter As String)
        'strFilter_Extern = strFilter
        ToolStripTextBox_Filter.Text = strFilter

    End Sub

    Private Sub Filter_List()
        strGUID_Filter = ""
        strName_Filter = ""
        objPropertyFilters = Nothing

        If ToolStripButton_Filter.Checked = True Then
            strName_Filter = ToolStripTextBox_Filter.Text
            If objLocalConfig.Globals.is_GUID(strName_Filter) Then
                strGUID_Filter = strName_Filter
                strName_Filter = ""
            End If
        End If

        If strFilter_Extern = "" Then
            configure_TabPages()
        Else
            Dim objOItem = New clsOntologyItem()
            Dim objPropertyFilter = New PropertyFilter()
            If strGUID_Filter <> "" Then
                objOItem.GUID = strGUID_Filter
                objPropertyFilter.PropertyName = strRowName_GUID
                objPropertyFilter.FilterObject = objOItem
                objPropertyFilters = New List(Of PropertyFilter) From {objPropertyFilter}
            ElseIf strName_Filter <> "" Then
                objOItem.Name = strName_Filter
                objPropertyFilter.PropertyName = strRowName_Name
                objPropertyFilter.FilterObject = objOItem
                objPropertyFilters = New List(Of PropertyFilter) From {objPropertyFilter}
            Else
                objPropertyFilters = Nothing
            End If

        End If

    End Sub

    Private Sub ToolStripButton_Filter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_Filter.Click
        If ToolStripButton_Filter.Checked = True Then
            ToolStripButton_Filter.Checked = False
            ToolStripTextBox_Filter.ReadOnly = True
            ToolStripTextBox_Filter.Text = ""
            ToolStripTextBox_Filter.ReadOnly = False
            Filter_List()
        Else
            ToolStripButton_Filter.Checked = True
            If strFilter_Extern <> "" Then
                ToolStripTextBox_Filter.ReadOnly = True
                ToolStripTextBox_Filter.Text = strFilter_Extern
                ToolStripTextBox_Filter.ReadOnly = False
            End If
            Filter_List()
        End If

    End Sub

    Private Sub Timer_List_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_List.Tick

        If boolFinished = True Then
            Timer_List.Stop()
            If Not objOItem_Parent Is Nothing Then
                Select Case objOItem_Parent.Type
                    Case objLocalConfig.Globals.Type_Object
                        ToolStripButton_FilterAdvanced.Enabled = True
                        If boolNullRelation Then
                            Dim objectsNotShown = objDBLevel.Objects1
                            'Dim objectsNotShown = objDBLevel.tbl_Objects.Rows.Cast(Of DataRow)()
                            If OItem_Direction_AdvancedFilter.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                                Dim objects = objDBLevel2.Objects1
                                objects.AddRange(From objectShow In objDBLevel2.Objects1
                                               Group Join objectNotShow In objectsNotShown On objectShow.GUID Equals objectNotShow.GUID Into objectsNotShow = Group
                                               From objectNotShow In objectsNotShow.DefaultIfEmpty()
                                               Where objectNotShow Is Nothing
                                               Select objectShow)
                                DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objects)
                            Else
                                Dim objects = objDBLevel2.Objects2
                                objects.AddRange(From objectShow In objDBLevel2.Objects2
                                               Group Join objectNotShow In objectsNotShown On objectShow.GUID Equals objectNotShow.GUID Into objectsNotShow = Group
                                               From objectNotShow In objectsNotShow.DefaultIfEmpty()
                                               Where objectNotShow Is Nothing
                                               Select objectShow)
                                DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objects)
                            End If




                        Else
                            If Not OItem_Class_AdvancedFilter Is Nothing Then
                                If OItem_Direction_AdvancedFilter.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                                    DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.Objects1)
                                Else
                                    DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.Objects2)
                                End If
                            Else

                                DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.Objects1)

                            End If


                        End If

                        objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsOntologyItem))


                        DataGridView_Items.Columns(0).Visible = False
                        DataGridView_Items.Columns(2).Visible = False
                        DataGridView_Items.Columns(1).Width = DataGridView_Items.Width - 20
                        ToolStripLabel_Count.Text = DataGridView_Items.RowCount
                        strRowName_GUID = "ID_Item"
                    Case objLocalConfig.Globals.Type_RelationType

                        DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.RelationTypes)
                        DataGridView_Items.Columns(0).Visible = False
                        DataGridView_Items.Columns(1).Width = DataGridView_Items.Width - 20
                        ToolStripLabel_Count.Text = DataGridView_Items.RowCount
                        strRowName_GUID = "ID_Item"
                        objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsOntologyItem))
                    Case objLocalConfig.Globals.Type_AttributeType

                        DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.AttributeTypes)
                        DataGridView_Items.Columns(0).Visible = False
                        DataGridView_Items.Columns(1).Width = DataGridView_Items.Width - 20
                        ToolStripLabel_Count.Text = DataGridView_Items.RowCount
                        strRowName_GUID = "ID_Item"
                        objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsOntologyItem))
                End Select


            Else
                DataGridView_Items.DataSource = New SortableBindingList(Of clsObjectRel)(objDBLevel.ObjectRels)
                Select Case strType
                    Case objLocalConfig.Globals.Type_Object

                        DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.Objects1)
                        DataGridView_Items.Columns(0).Visible = False
                        DataGridView_Items.Columns(2).Visible = False
                        DataGridView_Items.Columns(1).Width = DataGridView_Items.Width - 20
                        ToolStripLabel_Count.Text = DataGridView_Items.RowCount
                        strRowName_GUID = "ID_Item"
                        objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsOntologyItem))
                    Case objLocalConfig.Globals.Type_RelationType

                        DataGridView_Items.DataSource = New SortableBindingList(Of clsOntologyItem)(objDBLevel.RelationTypes)
                        ToolStripLabel_Count.Text = DataGridView_Items.RowCount
                        strRowName_GUID = "ID_Item"
                        objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsOntologyItem))
                    Case objLocalConfig.Globals.Type_AttributeType

                        DataGridView_Items.DataSource = New SortableBindingList(Of clsObjectAtt)(objDBLevel.ObjAtts)
                        objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsObjectAtt))
                        ToolStripLabel_Count.Text = DataGridView_Items.RowCount
                        strRowName_GUID = "ID_ObjectAttribute"
                    Case objLocalConfig.Globals.Type_Other
                        If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                            objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsObjectRel), True)

                        Else
                            objGridConfigurator.ConfigureGruid(DataGridView_Items, GetType(clsObjectRel), False)

                        End If
                End Select

                RaiseEvent counted_Items(DataGridView_Items.RowCount)
                ToolStripLabel_Count.Text = DataGridView_Items.RowCount

            End If
            ToolStripProgressBar_List.Value = 0
            RaiseEvent ListDataFinished()
        Else
            ToolStripProgressBar_List.Value = 50

        End If
    End Sub

    Private Sub SaveObjects()
        Dim objOItem_Result As clsOntologyItem
        objOItem_Result = objTransaction_Objects.save_Object(objOItem_Parent.GUID_Parent)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            If objTransaction_Objects.Apply = True Then
                oList_Selected_Simple.AddRange(objTransaction_Objects.NameObjects)

                RaiseEvent applied_Items()
            Else
                configure_TabPages()
                If Not objTransaction_Objects.OItem_SavedLast Is Nothing Then
                    ToolStripTextBox_Filter.Text = objTransaction_Objects.OItem_SavedLast.GUID
                End If
            End If
            RaiseEvent addedSimpleItems(objTransaction_Objects.OList_ObjectsSaved)
        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            MsgBox("Beim Erzeugen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub Save_RelationTypes()
        Dim objOItem_Result As clsOntologyItem
        objOItem_Result = objTransaction_RelationTypes.save_RelType()
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            RaiseEvent addedSimpleItems(objTransaction_RelationTypes.OList_RelationTypesSaved)
            configure_TabPages()
        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Relation.GUID Then
            MsgBox("Es gibt bereits einen Beziehungstyp mit diesem Namen!", MsgBoxStyle.Exclamation)
        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            MsgBox("Beim Erzeugen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
        End If

    End Sub

    Private Sub SaveAttributeTypess()
        Dim objOItem_Result As clsOntologyItem
        objOItem_Result = objTransaction_AttributeTypes.save_AttType()
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            RaiseEvent addedSimpleItems(objTransaction_AttributeTypes.OList_AttributeTypesSaved)
            configure_TabPages()
        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Relation.GUID Then
            MsgBox("Es gibt bereits einen Beziehungstyp mit diesem Namen!", MsgBoxStyle.Exclamation)
        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            MsgBox("Beim Erzeugen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub SaveObjAtt()
        Dim oList_ObjAtt As New List(Of clsObjectAtt)
        Dim oList_AttType As New List(Of clsOntologyItem)
        Dim objOItem_Result As clsOntologyItem
        Dim boolValue As Boolean
        Dim dateValue As Date
        Dim lngValue As Long
        Dim dblValue As Double
        Dim strValue As String
        Dim strVal_Named As String

        oList_AttType.Add(New clsOntologyItem(objOItem_Other.GUID, objLocalConfig.Globals.Type_AttributeType))
        objDBLevel.GetDataAttributeType(oList_AttType, False)


        Dim objLAT = From objAT In objDBLevel.AttributeTypes
                     Group By objAT.GUID, objAT.Name, objAT.GUID_Parent Into Group
        If objLAT.Count > 0 Then
            Select Case objLAT(0).GUID_Parent
                Case objLocalConfig.Globals.DType_Bool.GUID
                    objDlg_Attribute_Boolean = New dlg_Attribute_Boolean(objLAT(0).Name & "/" & objLocalConfig.Globals.DType_Bool.Name, objLocalConfig)
                    objDlg_Attribute_Boolean.ShowDialog(Me)
                    If objDlg_Attribute_Boolean.DialogResult = DialogResult.OK Then
                        boolValue = objDlg_Attribute_Boolean.Value
                        If boolValue = True Then
                            strVal_Named = "True"
                        Else
                            strVal_Named = "False"
                        End If

                        oList_ObjAtt.Clear()
                        oList_ObjAtt.Add(New clsObjectAtt(Nothing, _
                                                           objOItem_Object.GUID, _
                                                           objOItem_Object.Name, _
                                                           objOItem_Object.GUID_Parent, _
                                                           Nothing, _
                                                           objOItem_Other.GUID, _
                                                           Nothing, _
                                                           1, _
                                                           strVal_Named, _
                                                           boolValue, _
                                                           Nothing, _
                                                           Nothing, _
                                                           Nothing, _
                                                           Nothing, _
                                                           objLocalConfig.Globals.DType_Bool.GUID))

                        objOItem_Result = objDBLevel.SaveObjAtt(oList_ObjAtt)
                        If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            MsgBox("Leider konnte das Attribut nicht gespeichert werden!", MsgBoxStyle.Exclamation)

                        End If
                        configure_TabPages()
                    End If
                Case objLocalConfig.Globals.DType_DateTime.GUID
                    objDlg_Attribute_DateTime = New dlg_Attribute_DateTime(objLAT(0).Name & "/" & objLocalConfig.Globals.DType_DateTime.Name, objLocalConfig)
                    objDlg_Attribute_DateTime.ShowDialog(Me)
                    If objDlg_Attribute_DateTime.DialogResult = DialogResult.OK Then
                        dateValue = objDlg_Attribute_DateTime.Value

                        strVal_Named = dateValue.ToString
                        oList_ObjAtt.Clear()
                        oList_ObjAtt.Add(New clsObjectAtt(Nothing, _
                                                           objOItem_Object.GUID, _
                                                           objOItem_Object.Name, _
                                                           objOItem_Object.GUID_Parent, _
                                                           Nothing, _
                                                           objOItem_Other.GUID, _
                                                           Nothing, _
                                                           1, _
                                                           strVal_Named, _
                                                           Nothing, _
                                                           dateValue, _
                                                           Nothing, _
                                                           Nothing, _
                                                           Nothing, _
                                                           objLocalConfig.Globals.DType_DateTime.GUID))

                        objOItem_Result = objDBLevel.SaveObjAtt(oList_ObjAtt)
                        If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            MsgBox("Leider konnte das Attribut nicht gespeichert werden!", MsgBoxStyle.Exclamation)

                        End If
                        configure_TabPages()
                    End If
                Case objLocalConfig.Globals.DType_Int.GUID
                    objDlg_Attribute_Long = New dlg_Attribute_Long(objLAT(0).Name & "/" & objLocalConfig.Globals.DType_Int.Name, objLocalConfig)
                    objDlg_Attribute_Long.ShowDialog(Me)
                    If objDlg_Attribute_Long.DialogResult = DialogResult.OK Then
                        lngValue = objDlg_Attribute_Long.Value

                        strVal_Named = lngValue.ToString
                        oList_ObjAtt.Clear()
                        oList_ObjAtt.Add(New clsObjectAtt(Nothing, _
                                                           objOItem_Object.GUID, _
                                                           objOItem_Object.Name, _
                                                           objOItem_Object.GUID_Parent, _
                                                           Nothing, _
                                                           objOItem_Other.GUID, _
                                                           Nothing, _
                                                           1, _
                                                           strVal_Named, _
                                                           Nothing, _
                                                           Nothing, _
                                                           lngValue, _
                                                           Nothing, _
                                                           Nothing, _
                                                           objLocalConfig.Globals.DType_Int.GUID))

                        objOItem_Result = objDBLevel.SaveObjAtt(oList_ObjAtt)
                        If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            MsgBox("Leider konnte das Attribut nicht gespeichert werden!", MsgBoxStyle.Exclamation)

                        End If
                        configure_TabPages()
                    End If
                Case objLocalConfig.Globals.DType_Real.GUID
                    objDlg_Attribute_Double = New dlg_Attribute_Double(objLAT(0).Name & "/" & objLocalConfig.Globals.DType_Real.Name, objLocalConfig)
                    objDlg_Attribute_Double.ShowDialog(Me)
                    If objDlg_Attribute_Double.DialogResult = DialogResult.OK Then
                        dblValue = objDlg_Attribute_Double.Value

                        strVal_Named = dblValue.ToString
                        oList_ObjAtt.Clear()
                        oList_ObjAtt.Add(New clsObjectAtt(Nothing, _
                                                           objOItem_Object.GUID, _
                                                           objOItem_Object.Name, _
                                                           objOItem_Object.GUID_Parent, _
                                                           Nothing, _
                                                           objOItem_Other.GUID, _
                                                           Nothing, _
                                                           1, _
                                                           strVal_Named, _
                                                           Nothing, _
                                                           Nothing, _
                                                           Nothing, _
                                                           dblValue, _
                                                           Nothing, _
                                                           objLocalConfig.Globals.DType_Real.GUID))

                        objOItem_Result = objDBLevel.SaveObjAtt(oList_ObjAtt)
                        If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            MsgBox("Leider konnte das Attribut nicht gespeichert werden!", MsgBoxStyle.Exclamation)

                        End If
                        configure_TabPages()
                    End If
                Case objLocalConfig.Globals.DType_String.GUID
                    objDlg_Attribute_String = New dlg_Attribute_String(objLAT(0).Name & "/" & objLocalConfig.Globals.DType_String.Name, objLocalConfig)
                    objDlg_Attribute_String.ShowDialog(Me)
                    If objDlg_Attribute_String.DialogResult = DialogResult.OK Then
                        strValue = objDlg_Attribute_String.Value

                        strVal_Named = strValue
                        oList_ObjAtt.Clear()
                        oList_ObjAtt.Add(New clsObjectAtt(Nothing, _
                                                           objOItem_Object.GUID, _
                                                           objOItem_Object.Name, _
                                                           objOItem_Object.GUID_Parent, _
                                                           Nothing, _
                                                           objOItem_Other.GUID, _
                                                           Nothing, _
                                                           1, _
                                                           strVal_Named, _
                                                           Nothing, _
                                                           Nothing, _
                                                           Nothing, _
                                                           Nothing, _
                                                           strValue, _
                                                           objLocalConfig.Globals.DType_String.GUID))

                        objOItem_Result = objDBLevel.SaveObjAtt(oList_ObjAtt)
                        If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            MsgBox("Leider konnte das Attribut nicht gespeichert werden!", MsgBoxStyle.Exclamation)

                        End If
                        configure_TabPages()
                    End If
            End Select

        End If
    End Sub

    Private Sub ToolStripButton_AddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_AddItem.Click
        Dim objOItem_Result As clsOntologyItem
        Dim objOItem_Class As New clsOntologyItem
        Dim oList_Simple As List(Of clsOntologyItem)
        Dim oList_ObjRel As New List(Of clsObjectRel)
        Dim oItem_Obj As clsOntologyItem
        Dim objOItem_ClipBoardEntry As clsOntologyItem
        Dim boolAdd As Boolean
        Dim boolCancel As Boolean



        Dim boolOpenMain As Boolean

        If HandOff_Add Then
            objFrm_Main = New frmMain(objLocalConfig.Globals)
            objFrm_Main.Applyable = True
            objFrm_Main.ShowDialog(Me)
            If objFrm_Main.DialogResult = DialogResult.OK Then
                If objFrm_Main.OList_Simple.Any Then
                    oList_Simple = objFrm_Main.OList_Simple
                    RaiseEvent addedHandOffItems(oList_Simple)
                Else
                    MsgBox("Bitte ein Element auswählen!", MsgBoxStyle.Information)
                End If
            End If

            objFrm_Main.Dispose()
        Else

            If Not objOItem_Parent Is Nothing Then
                Select Case objOItem_Parent.Type
                    Case objLocalConfig.Globals.Type_Object
                        SaveObjects()

                    Case objLocalConfig.Globals.Type_RelationType
                        Save_RelationTypes()

                    Case objLocalConfig.Globals.Type_AttributeType
                        SaveAttributeTypess()


                End Select


            Else

                Select Case strType
                    Case objLocalConfig.Globals.Type_Object


                    Case objLocalConfig.Globals.Type_RelationType



                    Case objLocalConfig.Globals.Type_AttributeType
                        SaveObjAtt()


                    Case objLocalConfig.Globals.Type_Other
                        boolCancel = False
                        Dim boolOrderID = False
                        If objOItem_Other Is Nothing Then
                            objFrm_Clipboard = New frmClipboard(objLocalConfig)
                            Dim objOLRel As New List(Of clsObjectRel)
                            If objFrm_Clipboard.containedByClipboard() = True Then
                                objFrm_Clipboard.ShowDialog(Me)
                                boolOR = objFrm_Clipboard.OrderID
                                If objFrm_Clipboard.DialogResult = DialogResult.OK Then
                                    For Each objDGVR_Selected As DataGridViewRow In objFrm_Clipboard.selectedRows
                                        objOLRel.Add(objDGVR_Selected.DataBoundItem)

                                    Next
                                End If
                            End If

                            If objFrm_Clipboard.Cntrl = False Then
                                If Not objOLRel.Any Then
                                    objFrm_Main = New frmMain(objLocalConfig.Globals)
                                    objFrm_Main.Applyable = True
                                    objFrm_Main.ShowDialog(Me)
                                    If objFrm_Main.DialogResult = DialogResult.OK Then
                                        If objFrm_Main.OList_Simple.Any Then
                                            oList_Simple = objFrm_Main.OList_Simple
                                            boolAdd = True
                                        Else
                                            MsgBox("Bitte ein Element auswählen!", MsgBoxStyle.Information)
                                        End If
                                    Else
                                        boolCancel = True
                                    End If

                                    objFrm_Main.Dispose()
                                Else
                                    oList_Simple = (From objORel In objOLRel
                                                            Select New clsOntologyItem With {.GUID = objORel.ID_Other, _
                                                                                             .Name = objORel.Name_Other, _
                                                                                             .GUID_Parent = objORel.ID_Parent_Other, _
                                                                                             .Type = objLocalConfig.Globals.Type_Object, _
                                                                                             .Level = objORel.OrderID, _
                                                                                             .Mark = boolOrderID}).ToList()
                                    boolAdd = True
                                End If
                            Else
                                boolCancel = True
                            End If


                        Else
                            Select Case objOItem_Other.Type
                                Case objLocalConfig.Globals.Type_Object



                                    If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                                        objOItem_ClipBoardEntry = New clsOntologyItem(Nothing, Nothing, objOItem_Other.GUID_Parent, objLocalConfig.Globals.Type_Object)
                                        objOItem_Class.GUID = objOItem_Other.GUID_Parent
                                        objOItem_Class.Type = objLocalConfig.Globals.Type_Class
                                    Else
                                        objOItem_ClipBoardEntry = New clsOntologyItem(Nothing, Nothing, strGUID_Class, objLocalConfig.Globals.Type_Object)
                                        objOItem_Class.GUID = strGUID_Class
                                        objOItem_Class.Type = objLocalConfig.Globals.Type_Class
                                    End If

                                    boolOpenMain = True

                                    objFrm_Clipboard = New frmClipboard(objLocalConfig, objOItem_ClipBoardEntry)
                                    Dim objOLRel As New List(Of clsObjectRel)
                                    If objFrm_Clipboard.containedByClipboard() = True Then
                                        objFrm_Clipboard.ShowDialog(Me)
                                        boolOrderID = objFrm_Clipboard.OrderID
                                        If objFrm_Clipboard.DialogResult = DialogResult.OK Then
                                            For Each objDGVR_Selected As DataGridViewRow In objFrm_Clipboard.selectedRows
                                                objOLRel.Add(objDGVR_Selected.DataBoundItem)

                                            Next
                                        End If
                                    End If

                                    If objFrm_Clipboard.Cntrl = False Then
                                        If Not objOLRel.Any Then
                                            objFrm_Main = New frmMain(objLocalConfig, objLocalConfig.Globals.Type_Class, objOItem_Class)
                                            objFrm_Main.ShowDialog(Me)
                                            If objFrm_Main.DialogResult = DialogResult.OK Then
                                                If objFrm_Main.Type_Applied = objLocalConfig.Globals.Type_Object Then
                                                    oList_Simple = objFrm_Main.OList_Simple
                                                    boolAdd = True

                                                    Dim oLSel = From obj In oList_Simple
                                                                Group By obj.GUID_Parent Into Group

                                                    For Each oSel In oLSel
                                                        If Not oSel.GUID_Parent = objOItem_Class.GUID Then
                                                            boolAdd = False
                                                            Exit For
                                                        End If
                                                    Next

                                                Else
                                                    MsgBox("Bitte nur Objekte auswählen!", MsgBoxStyle.Information)
                                                End If

                                            Else
                                                boolCancel = True
                                            End If

                                            objFrm_Main.Dispose()
                                        Else
                                            oList_Simple = (From objORel In objOLRel
                                                            Select New clsOntologyItem With {.GUID = objORel.ID_Other, _
                                                                                             .Name = objORel.Name_Other, _
                                                                                             .GUID_Parent = objORel.ID_Parent_Other, _
                                                                                             .Type = objLocalConfig.Globals.Type_Object, _
                                                                                             .Level = objORel.OrderID, _
                                                                                             .Mark = boolOrderID}).ToList()
                                            boolAdd = True
                                        End If
                                    Else
                                        boolCancel = True
                                    End If


                            End Select
                        End If


                        If boolAdd = True Then
                            For Each oItem_Obj In oList_Simple
                                If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                                    oList_ObjRel.Add(New clsObjectRel(objOItem_Object.GUID, objOItem_Object.GUID_Parent, oItem_Obj.GUID, oItem_Obj.GUID_Parent, objOItem_RelationType.GUID, oItem_Obj.Type, Nothing, If(oItem_Obj.Mark, oItem_Obj.Level, 1)))

                                Else
                                    oList_ObjRel.Add(New clsObjectRel(oItem_Obj.GUID, oItem_Obj.GUID_Parent, objOItem_Other.GUID, objOItem_Other.GUID_Parent, objOItem_RelationType.GUID, oItem_Obj.Type, Nothing, If(oItem_Obj.Mark, oItem_Obj.Level, 1)))

                                End If
                            Next
                            objOItem_Result = objDBLevel.SaveObjRel(oList_ObjRel)

                            If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                MsgBox("Beim Speichern ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                            ElseIf objOItem_Result.Max1 > objOItem_Result.Val_Long Then
                                MsgBox("Es konnten nicht alle Beziehungen erzeugt werden!", MsgBoxStyle.Information)

                            End If
                            configure_TabPages()
                        Else
                            If boolCancel = False Then
                                MsgBox("Sie haben Objekte der falschen Klasse ausgewählt!", MsgBoxStyle.Exclamation)
                            End If

                        End If


                End Select

            End If
        End If

    End Sub


    Private Sub ToolStripButton_DelItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_DelItem.Click
        Dim objDGVR_Selected As DataGridViewRow
        Dim oList_Simple As New List(Of clsOntologyItem)
        Dim oList_ORel As New List(Of clsObjectRel)
        Dim oList_OAtt As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem
        Dim objOAL_ObjectAtts As New List(Of clsObjectAtt)

        For Each objDGVR_Selected In DataGridView_Items.SelectedRows
            Dim objItem = objDGVR_Selected.DataBoundItem

            If TypeOf (objItem) Is clsOntologyItem Then
                Dim objOItem = CType(objItem, clsOntologyItem)

                oList_Simple.Add(New clsOntologyItem(objOItem.GUID, objOItem.Type))
            ElseIf TypeOf (objItem) Is clsObjectAtt Then
                Dim objOAItem = CType(objItem, clsObjectAtt)
                oList_OAtt.Add(New clsObjectAtt With {.ID_Attribute = objOAItem.ID_Attribute})

            ElseIf TypeOf (objItem) Is clsObjectRel Then
                Dim objORel = CType(objItem, clsObjectRel)
                oList_ORel.Add(New clsObjectRel With {.ID_Object = objORel.ID_Object,
                                                      .ID_Other = objORel.ID_Other,
                                                      .ID_RelationType = objORel.ID_RelationType})
            End If

        Next

        If Not oList_Simple Is Nothing Then
            If oList_Simple.Count > 0 Then

                If oList_Simple(0).Type = objLocalConfig.Globals.Type_AttributeType Then
                    objOItem_Result = objDBLevel.DelAttributeTypes(oList_Simple)
                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                        If objOItem_Result.Count > 0 Then
                            MsgBox("Es konnten nur " & objOItem_Result.Min & " von " & objOItem_Result.Max1 & " Items gelöscht werden!", MsgBoxStyle.Information)
                        End If

                    ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                        MsgBox("Beim Löschen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                    End If
                    get_Data()
                ElseIf oList_Simple(0).Type = objLocalConfig.Globals.Type_Object Then
                    If Control.ModifierKeys = Keys.Control Then
                        If MsgBox("Wollen Sie wirklich alle markierten Elemente einschließlich derer Beziehungen löschen?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            Dim objOList_AttributesDel = oList_Simple.Select(Function(o) New clsObjectAtt With {.ID_Object = o.GUID}).ToList()
                            Dim objOList_ObjectsForw = oList_Simple.Select(Function(o) New clsObjectRel With {.ID_Object = o.GUID}).ToList()
                            Dim objOList_ObjectsBackw = oList_Simple.Select(Function(o) New clsObjectRel With {.ID_Other = o.GUID}).ToList()


                            objOItem_Result = objDBLevel.DelObjectAtts(objOList_AttributesDel)
                            If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                objOItem_Result = objDBLevel.DelObjectRels(objOList_ObjectsForw)
                                If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                    objOItem_Result = objDBLevel.DelObjectRels(objOList_ObjectsBackw)
                                    If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                        objOItem_Result = objDBLevel.DelObjects(oList_Simple)
                                        If objOItem_Result.Count > 0 Then
                                            MsgBox("Es konnten nur " & objOItem_Result.Min & " von " & objOItem_Result.Max1 & " Items gelöscht werden!", MsgBoxStyle.Information)
                                        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                            MsgBox("Beim Löschen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                                        End If
                                    Else
                                        MsgBox("Die Elemente konnten nicht gelöscht werden!", MsgBoxStyle.Exclamation)
                                    End If
                                Else
                                    MsgBox("Die Elemente konnten nicht gelöscht werden!", MsgBoxStyle.Exclamation)
                                End If
                            Else
                                MsgBox("Die Elemente konnten nicht gelöscht werden!", MsgBoxStyle.Exclamation)
                            End If
                        End If

                    Else
                        objOItem_Result = objDBLevel.DelObjects(oList_Simple)
                        If objOItem_Result.Count > 0 Then
                            MsgBox("Es konnten nur " & objOItem_Result.Min & " von " & objOItem_Result.Max1 & " Items gelöscht werden!", MsgBoxStyle.Information)
                        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Beim Löschen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                        End If

                    End If
                    get_Data()

                ElseIf oList_Simple(0).Type = objLocalConfig.Globals.Type_RelationType Then
                    objOItem_Result = objDBLevel.DelRelationTypes(oList_Simple)
                    If objOItem_Result.Count > 0 Then
                        MsgBox("Es konnten nur " & objOItem_Result.Min & " von " & objOItem_Result.Max1 & " Items gelöscht werden!", MsgBoxStyle.Information)
                    ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                        MsgBox("Beim Löschen ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                    End If
                    get_Data()
                End If





            End If
        End If

        If Not oList_ORel Is Nothing Then
            If oList_ORel.Count > 0 Then
                objOItem_Result = objDBLevel.DelObjectRels(oList_ORel)
                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                    MsgBox("Die Beziehungen konnten nicht entfernt werden!", MsgBoxStyle.Exclamation)
                End If
            End If
        End If

        If Not oList_OAtt Is Nothing Then
            If oList_OAtt.Count > 0 Then
                objOItem_Result = objDBLevel.DelObjectAtts(oList_OAtt)
                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                    MsgBox("Die Beziehungen konnten nicht entfernt werden!", MsgBoxStyle.Exclamation)
                End If
            End If
        End If

        configure_TabPages()
    End Sub

    Private Sub ContextMenuStrip_SemList_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip_SemList.Opening
        Dim i As Integer
        Dim boolFillCombo As Boolean = False

        ToClipboardToolStripMenuItem.Enabled = False
        ApplyToolStripMenuItem.Enabled = False
        DuplicateItemToolStripMenuItem.Enabled = False
        ChangeOrderIDsToolStripMenuItem.Enabled = False
        MoveObjectsToolStripMenuItem.Enabled = False
        RelateToItemByNameToolStripMenuItem.Enabled = False
        OpenModuleByArgumentToolStripMenuItem.Enabled = False
        If DataGridView_Items.SelectedRows.Count > 0 Then
            If Not objOItem_Parent Is Nothing Then
                If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                    DuplicateItemToolStripMenuItem.Enabled = True
                End If
            End If
            If boolApplyable = True Then
                ApplyToolStripMenuItem.Enabled = True
            End If
            If strType = objLocalConfig.Globals.Type_Object Then
                RelateToItemByNameToolStripMenuItem.Enabled = True
            End If

            If DataGridView_Items.SelectedRows.Count > 0 Then
                If Not objOItem_Parent Is Nothing Then
                    If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                        MoveObjectsToolStripMenuItem.Enabled = True
                        OpenModuleByArgumentToolStripMenuItem.Enabled = True
                    End If
                Else
                    If Not objOItem_Direction Is Nothing Then
                        OpenModuleByArgumentToolStripMenuItem.Enabled = True
                    End If
                End If


            End If

            If Not objOItem_Parent Is Nothing Then
                If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                    OpenModuleByArgumentToolStripMenuItem.Enabled = True
                    MoveObjectsToolStripMenuItem.Enabled = True
                End If
            End If
            ToClipboardToolStripMenuItem.Enabled = True
        End If

        If DataGridView_Items.SelectedCells.Count > 0 Then
            ChangeOrderIDsToolStripMenuItem.Enabled = True
        End If
    End Sub

    Private Sub TestFrmOItemList()


        Dim objOItemList = New List(Of clsOntologyItem)

        Try
            For Each objDGVR As DataGridViewRow In DataGridView_Items.SelectedRows

                Dim objItem = objDGVR.DataBoundItem
                If TypeOf (objItem) Is clsOntologyItem Then
                    objOItemList.Add(CType(objItem, clsOntologyItem))
                End If



            Next

            Dim objVisCol = New List(Of String) From {"GUID", "Name", "GUID_Parent"}

            objFrmOItemListToShow = New frmOntologyItemList(objOItemList, objLocalConfig.Globals, objVisCol)
            objFrmOItemListToShow.ShowDialog(Me)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ApplyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ApplyToolStripMenuItem.Click
        Dim objDGVR_Selected As DataGridViewRow

        oList_Selected_ObjectRel.Clear()
        oList_Selected_Simple.Clear()
        If boolApplyable = True And DataGridView_Items.SelectedRows.Count > 0 Then
            For Each objDGVR_Selected In DataGridView_Items.SelectedRows
                Dim objOItem = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)

                If Not objOItem_Parent Is Nothing Then
                    Select Case objOItem_Parent.Type
                        Case objLocalConfig.Globals.Type_Object
                            oList_Selected_Simple.Add(objOItem)

                        Case objLocalConfig.Globals.Type_RelationType
                            oList_Selected_Simple.Add(objOItem)
                        Case objLocalConfig.Globals.Type_AttributeType
                            oList_Selected_Simple.Add(objOItem)
                    End Select


                Else

                    Select Case strType
                        Case objLocalConfig.Globals.Type_Object


                        Case objLocalConfig.Globals.Type_RelationType



                        Case objLocalConfig.Globals.Type_AttributeType



                        Case objLocalConfig.Globals.Type_Other


                    End Select

                End If
            Next

            If ToolStripMenuItem_ChooseModule.Checked And Not String.IsNullOrEmpty(strGuidApplyModule) Then
                If oList_Selected_Simple.Any() Then
                    Dim applyItems = oList_Selected_Simple.Select(Function(objItem) New clsApplyItem With {.IdItem = objItem.GUID,
                                                                      .NameItem = objItem.Name,
                                                                      .IdItemParent = objItem.GUID_Parent,
                                                                      .Type = objItem.Type}).ToArray()

                    Dim objOItemResult = ModuleDataExchanger.Client(strGuidApplyModule, applyItems)

                End If
            End If

            RaiseEvent applied_Items()
        End If
    End Sub

    Private Sub ToClipboardToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToClipboardToolStripMenuItem.Click
        Dim objDGVR_Selected As DataGridViewRow
        Dim objOItem_ToClipboard As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem

        For Each objDGVR_Selected In DataGridView_Items.SelectedRows
            If TypeOf (objDGVR_Selected.DataBoundItem) Is clsOntologyItem Then
                objOItem_ToClipboard = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)
            Else
                Dim objORel = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)
                If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                    objOItem_ToClipboard = New clsOntologyItem With {.GUID = objORel.ID_Other,
                                                                     .Name = objORel.Name_Other,
                                                                     .GUID_Parent = objORel.ID_Parent_Other,
                                                                     .Type = objORel.Ontology}
                Else
                    objOItem_ToClipboard = New clsOntologyItem With {.GUID = objORel.ID_Object,
                                                                     .Name = objORel.Name_Object,
                                                                     .GUID_Parent = objORel.ID_Parent_Object,
                                                                     .Type = objORel.Ontology}
                End If
            End If

            objOItem_Result = objOntologyClipboard.addToClipboard(objOItem_ToClipboard, False)
            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                MsgBox("Das Item kann nicht ins Clipboard geschrieben werden!", MsgBoxStyle.Exclamation)

            End If
        Next
    End Sub

    Private Sub CopyNameToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopyNameToolStripMenuItem.Click
        Dim objDGVR_Selected As DataGridViewRow
        Dim strClipboard As String = ""


        For Each objDGVR_Selected In DataGridView_Items.SelectedRows

            If Not objOItem_Parent Is Nothing Then
                Dim objOItem = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)
                strClipboard = objOItem.Name


            Else

                Select Case strType
                    Case objLocalConfig.Globals.Type_Object


                    Case objLocalConfig.Globals.Type_RelationType



                    Case objLocalConfig.Globals.Type_AttributeType



                    Case objLocalConfig.Globals.Type_Other
                        Dim objOItem = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)
                        If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then

                            strClipboard = objOItem.Name_Other


                        Else
                            strClipboard = objOItem.Name_Object

                        End If



                End Select

            End If
        Next

        Clipboard.SetDataObject(strClipboard)
    End Sub

    Private Sub DuplicateItemToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DuplicateItemToolStripMenuItem.Click
        Dim objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone()

        For Each objDGVR_Selected As DataGridViewRow In DataGridView_Items.SelectedRows
            Dim objOItem_Object = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)

            objOItem_Result = objTransaction_Objects.duplicate_Object(objOItem_Object)

            If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                Exit For
            End If
        Next

        If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Nothing.GUID Then
            configure_TabPages()
            ToolStripTextBox_Filter.Text = objTransaction_Objects.OItem_SavedLast.GUID
        End If

    End Sub

    Private Sub ToolStripButton_FilterAdvanced_Click(sender As Object, e As EventArgs) Handles ToolStripButton_FilterAdvanced.Click

        ToolStripButton_FilterAdvanced.Checked = False
        OItem_Class_AdvancedFilter = Nothing
        OItem_Object_AdvancedFilter = Nothing
        OItem_RelationType_AdvancedFilter = Nothing
        OItem_Direction_AdvancedFilter = Nothing
        boolNullRelation = False

        If strGUID_Class <> "" Then
            objOItem_Class.GUID = strGUID_Class
            objOItem_Class.Type = objLocalConfig.Globals.Type_Class

            objFrmAdvancedFilter = New frmAdvancedFilter(objLocalConfig, objOItem_Class)
            objFrmAdvancedFilter.ShowDialog(Me)
            If objFrmAdvancedFilter.DialogResult = DialogResult.OK Then
                OItem_Class_AdvancedFilter = objFrmAdvancedFilter.OItem_Class
                OItem_Object_AdvancedFilter = objFrmAdvancedFilter.OItem_Object
                OItem_RelationType_AdvancedFilter = objFrmAdvancedFilter.OItem_RelationType
                OItem_Direction_AdvancedFilter = objFrmAdvancedFilter.OItem_Direction

                boolNullRelation = objFrmAdvancedFilter.NullRelation

                ToolStripButton_FilterAdvanced.Checked = True
                get_Data()
                configure_TabPages()
            End If
        End If


    End Sub

    Public Sub Initialize_AdvancedFilter(Optional OItem_Class_AdvancedFilter = Nothing,
                                         Optional OItem_Object_AdvancedFilter = Nothing,
                                         Optional OItem_RelationType_AdvancedFilter = Nothing,
                                         Optional OItem_Direction_AdvancedFilter = Nothing)

        If Not OItem_Class_AdvancedFilter Is Nothing Then
            Me.OItem_Class_AdvancedFilter = OItem_Class_AdvancedFilter
        End If

        If Not OItem_Object_AdvancedFilter Is Nothing Then
            Me.OItem_Object_AdvancedFilter = OItem_Object_AdvancedFilter
        End If

        If Not OItem_RelationType_AdvancedFilter Is Nothing Then
            Me.OItem_RelationType_AdvancedFilter = OItem_RelationType_AdvancedFilter
        End If

        If Not OItem_Direction_AdvancedFilter Is Nothing Then
            Me.OItem_Direction_AdvancedFilter = OItem_Direction_AdvancedFilter
        End If
        ToolStripButton_FilterAdvanced.Checked = True
        get_Data()
        configure_TabPages()
    End Sub

    Private Sub ToolStripButton_Replace_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Replace.Click
        If strType = objLocalConfig.Globals.Type_Object Then
            Dim objObjectList As List(Of clsOntologyItem) = DataGridView_Items.SelectedRows.Cast(Of DataGridViewRow).Select(Function(row) CType(row.DataBoundItem, clsOntologyItem)).ToList()

            If objObjectList.Any() Then
                objFrm_Replace = New frmReplace(objLocalConfig, objObjectList)
                objFrm_Replace.ShowDialog(Me)
                If objFrm_Replace.DialogResult = DialogResult.OK Then
                    get_Data()
                End If

            End If
        End If
    End Sub

    Private Sub ModuleMenuToolStripMenuItem_DropDownOpening(sender As Object, e As EventArgs) Handles ModuleMenuToolStripMenuItem.DropDownOpening

        If Not objOItem_Parent Is Nothing Then
            If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                If DataGridView_Items.SelectedRows.Count = 1 Then
                    Dim objDGVR_Selected As DataGridViewRow = DataGridView_Items.SelectedRows(0)
                    Dim objOItem_Object = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)

                    If Not objLocalConfig.Globals.ModuleList Is Nothing Then
                        For Each objModule In objLocalConfig.Globals.ModuleList

                            Dim objMenuItems = objModule.GetMenuItems(objOItem_Object)
                            If Not objMenuItems Is Nothing Then
                                Dim objToolStrip_Root As ToolStripMenuItem
                                For i As Integer = 0 To objMenuItems.Count - 1
                                    If i = 0 Then
                                        If ModuleMenuToolStripMenuItem.DropDownItems.Count = 1 Then
                                            objToolStrip_Root = ModuleMenuToolStripMenuItem.DropDownItems.Add(objMenuItems(i).Name, Nothing)
                                        Else
                                            objToolStrip_Root = ModuleMenuToolStripMenuItem.DropDownItems(0)
                                        End If

                                    Else
                                        Dim boolAdd = True
                                        For Each objToolStripItem As ToolStripItem In objToolStrip_Root.DropDownItems
                                            If objToolStripItem.Text = objMenuItems(i).Name Then
                                                boolAdd = False
                                            End If
                                        Next

                                        If boolAdd Then
                                            objToolStrip_Root.DropDownItems.Add(objMenuItems(i).Name, Nothing, New EventHandler(AddressOf ModuleExecutor))
                                        End If
                                    End If


                                Next

                            End If
                        Next
                    End If
                    
                ElseIf DataGridView_Items.SelectedRows.Count > 1 Then

                End If
            End If

        ElseIf objOItem_Parent Is Nothing And Not objOItem_Other Is Nothing Then
            If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                If DataGridView_Items.SelectedRows.Count = 1 Then
                    Dim objDGVR_Selected As DataGridViewRow = DataGridView_Items.SelectedRows(0)
                    Dim objOItem_Object As clsOntologyItem
                    If TypeOf (objDGVR_Selected.DataBoundItem) Is clsOntologyItem Then
                        objOItem_Object = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)
                    Else
                        Dim objORel = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)
                        If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                            objOItem_Object = New clsOntologyItem With {.GUID = objORel.ID_Other,
                                                                        .Name = objORel.Name_Other,
                                                                        .GUID_Parent = objORel.ID_Parent_Other,
                                                                        .Type = objORel.Ontology}

                        Else
                            objOItem_Object = New clsOntologyItem With {.GUID = objORel.ID_Object,
                                                                        .Name = objORel.Name_Object,
                                                                        .GUID_Parent = objORel.ID_Parent_Object,
                                                                        .Type = objLocalConfig.Globals.Type_Object}
                        End If
                    End If

                    If Not objLocalConfig.Globals.ModuleList Is Nothing Then
                        For Each objModule In objLocalConfig.Globals.ModuleList

                            Dim objMenuItems = objModule.GetMenuItems(objOItem_Object)
                            If Not objMenuItems Is Nothing Then
                                Dim objToolStrip_Root As ToolStripMenuItem
                                For i As Integer = 0 To objMenuItems.Count - 1
                                    If i = 0 Then
                                        If ModuleMenuToolStripMenuItem.DropDownItems.Count = 1 Then
                                            objToolStrip_Root = ModuleMenuToolStripMenuItem.DropDownItems.Add(objMenuItems(i).Name, Nothing)
                                        Else
                                            objToolStrip_Root = ModuleMenuToolStripMenuItem.DropDownItems(0)
                                        End If

                                    Else
                                        Dim boolAdd = True
                                        For Each objToolStripItem As ToolStripItem In objToolStrip_Root.DropDownItems
                                            If objToolStripItem.Text = objMenuItems(i).Name Then
                                                boolAdd = False
                                            End If
                                        Next

                                        If boolAdd Then
                                            objToolStrip_Root.DropDownItems.Add(objMenuItems(i).Name, Nothing, New EventHandler(AddressOf ModuleExecutor))
                                        End If
                                    End If


                                Next

                            End If
                        Next
                    End If

                ElseIf DataGridView_Items.SelectedRows.Count > 1 Then


                End If
            Else
                If DataGridView_Items.SelectedRows.Count = 1 Then
                    Dim objDGVR_Selected As DataGridViewRow = DataGridView_Items.SelectedRows(0)
                    Dim objOItem_Object As clsOntologyItem
                    If TypeOf (objDGVR_Selected.DataBoundItem) Is clsOntologyItem Then
                        objOItem_Object = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)

                    Else
                        Dim objORel = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)
                        If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                            objOItem_Object = New clsOntologyItem With {.GUID = objORel.ID_Other,
                                                                        .Name = objORel.Name_Other,
                                                                        .GUID_Parent = objORel.ID_Parent_Other,
                                                                        .Type = objORel.Ontology}
                        Else
                            objOItem_Object = New clsOntologyItem With {.GUID = objORel.ID_Object,
                                                                        .Name = objORel.Name_Object,
                                                                        .GUID_Parent = objORel.ID_Parent_Object,
                                                                        .Type = objLocalConfig.Globals.Type_Object}
                        End If



                    End If
                    If Not objLocalConfig.Globals.ModuleList Is Nothing Then
                        For Each objModule In objLocalConfig.Globals.ModuleList

                            Dim objMenuItems = objModule.GetMenuItems(objOItem_Object)
                            If Not objMenuItems Is Nothing Then
                                Dim objToolStrip_Root As ToolStripMenuItem
                                For i As Integer = 0 To objMenuItems.Count - 1
                                    If i = 0 Then
                                        If ModuleMenuToolStripMenuItem.DropDownItems.Count = 1 Then
                                            objToolStrip_Root = ModuleMenuToolStripMenuItem.DropDownItems.Add(objMenuItems(i).Name, Nothing)
                                        Else
                                            objToolStrip_Root = ModuleMenuToolStripMenuItem.DropDownItems(0)
                                        End If

                                    Else
                                        Dim boolAdd = True
                                        For Each objToolStripItem As ToolStripItem In objToolStrip_Root.DropDownItems
                                            If objToolStripItem.Text = objMenuItems(i).Name Then
                                                boolAdd = False
                                            End If
                                        Next

                                        If boolAdd Then
                                            objToolStrip_Root.DropDownItems.Add(objMenuItems(i).Name, Nothing, New EventHandler(AddressOf ModuleExecutor))
                                        End If
                                    End If


                                Next

                            End If
                        Next
                    End If


                ElseIf DataGridView_Items.SelectedRows.Count > 1 Then


                End If
            End If

        End If








    End Sub

    Private Sub ModuleExecutor(sender As Object, e As EventArgs)

        If Not objOItem_Parent Is Nothing Then
            If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                If DataGridView_Items.SelectedRows.Count = 1 Then
                    Dim objMenuItem As ToolStripMenuItem = sender

                    Dim objDGVR_Selected As DataGridViewRow = DataGridView_Items.SelectedRows(0)
                    Dim objOItem_Object = CType(objDGVR_Selected.DataBoundItem, clsOntologyItem)

                    For Each objModule In objLocalConfig.Globals.ModuleList


                        Dim objMenuItems = objModule.GetMenuItems(objOItem_Object)
                        If objMenuItems.Any Then
                            If objMenuItems(0).Name = objMenuItem.OwnerItem.Text Then
                                Dim objMenuItemsSel = objMenuItems.Where(Function(m) m.Name = objMenuItem.Text).ToList()
                                If objMenuItemsSel.Any() Then
                                    Dim objOItem_MenuItem = objMenuItemsSel.First()
                                    objModule.Instance.Open_Viewer(objOItem_Object, objOItem_MenuItem)
                                End If

                            End If
                        End If
                    Next

                ElseIf DataGridView_Items.SelectedRows.Count > 1 Then

                End If

            End If
        ElseIf objOItem_Parent Is Nothing And Not objOItem_Other Is Nothing Then
            If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                If DataGridView_Items.SelectedRows.Count = 1 Then
                    Dim objMenuItem As ToolStripMenuItem = sender

                    Dim objDGVR_Selected As DataGridViewRow = DataGridView_Items.SelectedRows(0)
                    Dim objOItem As clsObjectRel = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)

                    Dim objOItem_Object = New clsOntologyItem With {.GUID = objOItem.ID_Other, _
                                                                    .Name = objOItem.Name_Other, _
                                                                    .GUID_Parent = objOItem.ID_Parent_Other, _
                                                                    .Type = objOItem.Ontology}

                    For Each objModule In objLocalConfig.Globals.ModuleList


                        Dim objMenuItems = objModule.GetMenuItems(objOItem_Object)
                        If objMenuItems.Any Then
                            If objMenuItems(0).Name = objMenuItem.OwnerItem.Text Then
                                Dim objMenuItemsSel = objMenuItems.Where(Function(m) m.Name = objMenuItem.Text).ToList()
                                If objMenuItemsSel.Any() Then
                                    Dim objOItem_MenuItem = objMenuItemsSel.First()
                                    objModule.Instance.Open_Viewer(objOItem_Object, objOItem_MenuItem)
                                End If

                            End If
                        End If
                    Next

                ElseIf DataGridView_Items.SelectedRows.Count > 1 Then

                End If
            Else
                If DataGridView_Items.SelectedRows.Count = 1 Then
                    Dim objMenuItem As ToolStripMenuItem = sender

                    Dim objDGVR_Selected As DataGridViewRow = DataGridView_Items.SelectedRows(0)
                    Dim objOItem = CType(objDGVR_Selected.DataBoundItem, clsObjectRel)

                    Dim objOItem_Object = New clsOntologyItem With {.GUID = objOItem.ID_Object, _
                                                                    .Name = objOItem.Name_Object, _
                                                                    .GUID_Parent = objOItem.ID_Parent_Object, _
                                                                    .Type = objLocalConfig.Globals.Type_Object}

                    For Each objModule In objLocalConfig.Globals.ModuleList


                        Dim objMenuItems = objModule.GetMenuItems(objOItem_Object)
                        If objMenuItems.Any Then
                            If objMenuItems(0).Name = objMenuItem.OwnerItem.Text Then
                                Dim objMenuItemsSel = objMenuItems.Where(Function(m) m.Name = objMenuItem.Text).ToList()
                                If objMenuItemsSel.Any() Then
                                    Dim objOItem_MenuItem = objMenuItemsSel.First()
                                    objModule.Instance.Open_Viewer(objOItem_Object, objOItem_MenuItem)
                                End If

                            End If
                        End If
                    Next

                ElseIf DataGridView_Items.SelectedRows.Count > 1 Then

                End If
            End If
        End If
    End Sub

    Private Sub ChangeOrderIDsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChangeOrderIDsToolStripMenuItem.Click
        Dim boolOk = DataGridView_Items.SelectedCells.Cast(Of DataGridViewCell)().All(Function(cell) DataGridView_Items.Columns(cell.ColumnIndex).DataPropertyName = "OrderID")
        Dim objOList_Relation As List(Of clsObjectRel) = New List(Of clsObjectRel)()
        Dim objOLIst_Att As List(Of clsObjectAtt) = New List(Of clsObjectAtt)()
        Dim boolRel As Boolean
        Dim boolChange = False

        If boolOk Then

            objDlg_Attribute_Long = New dlg_Attribute_Long("New OrderID", objLocalConfig.Globals, 0)
            objDlg_Attribute_Long.ShowDialog(Me)

            If objDlg_Attribute_Long.DialogResult = DialogResult.OK Then
                For Each objColumn In DataGridView_Items.Columns
                    If objColumn.DataPropertyName.ToLower = "id_other" Then
                        boolRel = True
                        boolChange = True
                        Exit For
                    End If

                    If objColumn.DataPropertyName.ToLower = "id_attribute" Then

                        boolRel = False
                        boolChange = True
                        Exit For
                    End If
                Next
                If boolChange Then
                    Dim lngOrderID = objDlg_Attribute_Long.Value
                    For Each cell As DataGridViewCell In DataGridView_Items.SelectedCells
                        Dim objDGVR As DataGridViewRow = DataGridView_Items.Rows(cell.RowIndex)



                        If boolRel Then
                            Dim objOItem_Rel = CType(objDGVR.DataBoundItem, clsObjectRel)
                            objOItem_Rel.OrderID = lngOrderID
                            objOList_Relation.Add(objOItem_Rel)


                        Else
                            Dim objOItem_Rel = CType(objDGVR.DataBoundItem, clsObjectAtt)
                            objOItem_Rel.OrderID = objDlg_Attribute_Long.Value
                            objOLIst_Att.Add(objOItem_Rel)


                        End If

                    Next

                    If boolRel And objOList_Relation.Any Then
                        Dim objOItem_Result = objDBLevel.SaveObjRel(objOList_Relation)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Die Gewichtung konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                        End If

                        get_Data()
                    ElseIf boolRel = False And objOLIst_Att.Any Then
                        Dim objOItem_Result = objDBLevel.SaveObjAtt(objOLIst_Att)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Die Gewichtung konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                        End If

                        get_Data()
                    End If

                Else
                    MsgBox("Hier können keine Order-IDs geändert werden!", MsgBoxStyle.OkOnly)
                End If




            End If
        Else
            MsgBox("Nur OrderID-Zellen können geändert werden!", MsgBoxStyle.OkOnly)

        End If
    End Sub

    Private Sub ToolStripButton_Sort_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Sort.Click
        Dim lngOrderID As Long
        Dim boolRel As Boolean
        Dim boolChange = False
        Dim objOList_Relation As List(Of clsObjectRel) = New List(Of clsObjectRel)()
        Dim objOLIst_Att As List(Of clsObjectAtt) = New List(Of clsObjectAtt)()

        If DataGridView_Items.SelectedRows.Count > 0 Then
            For Each objColumn In DataGridView_Items.Columns
                If objColumn.DataPropertyName.ToLower = "id_other" Then
                    boolRel = True
                    boolChange = True
                    Exit For
                End If

                If objColumn.DataPropertyName.ToLower = "id_attribute" Then

                    boolRel = False
                    boolChange = True
                    Exit For
                End If
            Next

            If boolChange Then
                objDlg_Attribute_Long = New dlg_Attribute_Long("New OrderID", objLocalConfig.Globals, 0)
                objDlg_Attribute_Long.ShowDialog(Me)

                If objDlg_Attribute_Long.DialogResult = DialogResult.OK Then
                    lngOrderID = objDlg_Attribute_Long.Value
                    For Each objDGVR As DataGridViewRow In DataGridView_Items.SelectedRows.Cast(Of DataGridViewRow).OrderBy(Function(i) i.Index).ToList()


                        If boolRel Then
                            Dim objOItem_Rel = CType(objDGVR.DataBoundItem, clsObjectRel)
                            objOItem_Rel.OrderID = lngOrderID
                            objOList_Relation.Add(objOItem_Rel)


                        Else
                            Dim objOItem_Rel = CType(objDGVR.DataBoundItem, clsObjectAtt)
                            objOItem_Rel.OrderID = objDlg_Attribute_Long.Value
                            objOLIst_Att.Add(objOItem_Rel)


                        End If
                        lngOrderID = lngOrderID + 1
                    Next

                    If boolRel And objOList_Relation.Any Then
                        Dim objOItem_Result = objDBLevel.SaveObjRel(objOList_Relation)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Die Gewichtung konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                        End If

                        get_Data()
                    ElseIf boolRel = False And objOLIst_Att.Any Then
                        Dim objOItem_Result = objDBLevel.SaveObjAtt(objOLIst_Att)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Die Gewichtung konnte nicht geändert werden!", MsgBoxStyle.Exclamation)
                        End If

                        get_Data()
                    End If
                End If
            Else
                MsgBox("Hier leider nicht möglich.", MsgBoxStyle.OkOnly)
            End If

        End If
    End Sub

    Private Sub MoveObjectsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MoveObjectsToolStripMenuItem.Click
        objFrm_Main = New frmMain(objLocalConfig)
        objFrm_Main.ShowDialog(Me)
        If objFrm_Main.DialogResult = DialogResult.OK Then
            If objFrm_Main.Type_Applied = objLocalConfig.Globals.Type_Class Then
                If objFrm_Main.OList_Simple.Count = 1 Then
                    Dim objOList_Objects_Orig = (From objDGVR As DataGridViewRow In DataGridView_Items.SelectedRows).Select(Function(r) CType(r.DataBoundItem, clsOntologyItem)).ToList()

                    Dim objOItem_Result = objTransaction_Objects.move_Objects(objOList_Objects_Orig, objFrm_Main.OList_Simple.First().GUID)




                    If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                        MsgBox("Die Objekte konnten nicht verschoben werden!", MsgBoxStyle.Exclamation)
                    End If

                    configure_TabPages()
                    objFrm_Main.Dispose()
                Else
                    MsgBox("Bitte wählen Sie eine Klasse aus!", MsgBoxStyle.OkOnly)
                End If

            Else
                MsgBox("Bitte wählen Sie eine Klasse aus!", MsgBoxStyle.OkOnly)
            End If
        End If
    End Sub

    Private Sub RelateToItemByNameToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RelateToItemByNameToolStripMenuItem.Click
        Dim objDGVR As DataGridViewRow = DataGridView_Items.SelectedRows(0)
        Dim objOItem = CType(objDGVR.DataBoundItem, clsOntologyItem)

        Dim objOItem_Object = New clsOntologyItem With {.GUID = objOItem.GUID_Parent, _
                                                        .Type = objLocalConfig.Globals.Type_Class}

        objFrm_RelationFilter = New frmRelationFilter(objLocalConfig, objOItem_Object)
        objFrm_RelationFilter.ShowDialog(Me)
        If objFrm_RelationFilter.DialogResult = DialogResult.OK Then
            Dim objORel_Objects As List(Of clsObjectRel)

            If objFrm_RelationFilter.OItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                objORel_Objects = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Parent_Object = objOItem.GUID_Parent, _
                                                         .ID_RelationType = objFrm_RelationFilter.OItem_RelationType.GUID, _
                                                         .ID_Parent_Other = objFrm_RelationFilter.OItem_Class.GUID}}




            Else
                objORel_Objects = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Parent_Other = objOItem.GUID_Parent, _
                                                         .ID_RelationType = objFrm_RelationFilter.OItem_RelationType.GUID, _
                                                         .ID_Parent_Object = objFrm_RelationFilter.OItem_Class.GUID}}
            End If

            Dim objOItem_Result = objDBLevel.GetDataObjectRel(objORel_Objects, doIds:=False)

            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                Dim objOItems_Object = (From objObject As DataGridViewRow In DataGridView_Items.Rows).Select(Function(row) CType(row.DataBoundItem, clsOntologyItem)).ToList()

                Dim objOItems_Related As List(Of clsObjectRel)


                If objFrm_RelationFilter.OItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                    objOItems_Related = (From objObject In objOItems_Object
                                         Join objRelation In objDBLevel.ObjectRels.Select(Function(rel) New clsOntologyItem With {.GUID = rel.ID_Other, _
                                                                                                                                       .Name = rel.Name_Other, _
                                                                                                                                       .GUID_Parent = rel.ID_Parent_Other, _
                                                                                                                                       .Type = objLocalConfig.Globals.Type_Object}).ToList() On objObject.Name.ToLower() Equals objRelation.Name.ToLower()
                                         Select objRelationConfig.Rel_ObjectRelation(objObject, objRelation, objFrm_RelationFilter.OItem_RelationType)).ToList()




                Else
                    objOItems_Related = (From objObject In objOItems_Object
                                         Join objRelation In objDBLevel.ObjectRels.Select(Function(rel) New clsOntologyItem With {.GUID = rel.ID_Other, _
                                                                                                                                       .Name = rel.Name_Other, _
                                                                                                                                       .GUID_Parent = rel.ID_Parent_Other, _
                                                                                                                                       .Type = objLocalConfig.Globals.Type_Object}).ToList() On objObject.Name.ToLower() Equals objRelation.Name.ToLower()
                                         Select objRelationConfig.Rel_ObjectRelation(objObject, objRelation, objFrm_RelationFilter.OItem_RelationType)).ToList()
                End If

                If (objOItems_Related.Any()) Then
                    objOItem_Result = objDBLevel.SaveObjRel(objOItems_Related)
                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                        If objOItems_Related.Count < objOItems_Object.Count Then
                            MsgBox("Es konnten nur " & objOItems_Related.Count & " von " & objOItems_Object.Count & " Beziehungen hergestellt werden!", MsgBoxStyle.Information)
                        Else
                            MsgBox("Alle Beziehungen wurden hergestellt!", MsgBoxStyle.Information)
                        End If
                    Else
                        MsgBox("Es konnten keine Beziehungen hergestellt werden!", MsgBoxStyle.Exclamation)
                    End If
                Else
                    MsgBox("Es wurden keine Beziehungen hergestellt!", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Die Liste der Objekte konnte nicht ermittelt werden!", MsgBoxStyle.Exclamation)
            End If
        End If
    End Sub

    Private Sub OpenModuleByArgumentToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenModuleByArgumentToolStripMenuItem.Click
        Dim objDGVR As DataGridViewRow = DataGridView_Items.SelectedRows(0)
        Dim objOItem_Object As clsOntologyItem = Nothing
        Dim objOItem_Session As clsOntologyItem = Nothing

        If DataGridView_Items.SelectedRows.Count = 1 Then
            If Not objOItem_Parent Is Nothing Then
                If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                    objOItem_Object = CType(objDGVR.DataBoundItem, clsOntologyItem)

                End If
            Else
                If Not objOItem_Direction Is Nothing Then
                    Dim objORel = CType(objDGVR.DataBoundItem, clsObjectRel)
                    If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then

                        If objORel.Ontology = objLocalConfig.Globals.Type_Object Then
                            objOItem_Object = New clsOntologyItem With {.GUID = objORel.ID_Other,
                                                        .Name = objORel.Name_Other,
                                                        .GUID_Parent = objORel.ID_Parent_Other,
                                                        .Type = objLocalConfig.Globals.Type_Object}
                        Else
                            MsgBox("Das Element in der Beziehung ist kein Objekt. Bitte wählen Sie nur Objekte.", MsgBoxStyle.Information)

                        End If

                    Else
                        objOItem_Object = New clsOntologyItem With {.GUID = objORel.ID_Object,
                                                        .Name = objORel.Name_Object,
                                                        .GUID_Parent = objORel.ID_Parent_Object,
                                                        .Type = objLocalConfig.Globals.Type_Object}
                    End If

                End If
            End If

        Else
            If Not objOItem_Parent Is Nothing Then
                If objOItem_Parent.Type = objLocalConfig.Globals.Type_Object Then
                    If objSession Is Nothing Then
                        objSession = New clsSession(objLocalConfig)
                    End If
                    Dim objectList = DataGridView_Items.SelectedRows.Cast(Of DataGridViewRow).Select(Function(row) CType(row.DataBoundItem, clsOntologyItem)).ToList()

                    objOItem_Session = objSession.RegisterSession()

                    If Not objOItem_Session Is Nothing Then
                        Dim objOItem_Result = objSession.RegisterItems(objOItem_Session, objectList, True)

                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            objOItem_Session = Nothing
                            MsgBox("Beim registrieren der Objekte ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                        End If
                    Else
                        MsgBox("Beim registrieren der Objekte ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                    End If
                End If
            Else
                Dim objORel = CType(objDGVR.DataBoundItem, clsObjectRel)
                If Not objOItem_Direction Is Nothing Then
                    If objOItem_Direction.GUID = objLocalConfig.Globals.Direction_LeftRight.GUID Then
                        If objORel.Ontology = objLocalConfig.Globals.Type_Object Then
                            If objSession Is Nothing Then
                                objSession = New clsSession(objLocalConfig)
                            End If

                            Dim objectList = DataGridView_Items.SelectedRows.Cast(Of DataGridViewRow).Select(Function(dgvr) CType(dgvr.DataBoundItem, clsObjectRel)).Select(Function(drv) New clsOntologyItem With {
                                                                                                                                                                        .GUID = drv.ID_Other, _
                                                                                                                                                                        .Name = drv.Name_Other, _
                                                                                                                                                                        .GUID_Parent = drv.ID_Parent_Other, _
                                                                                                                                                                        .Type = objLocalConfig.Globals.Type_Object}).ToList()
                            objOItem_Session = objSession.RegisterSession()

                            If Not objOItem_Session Is Nothing Then
                                Dim objOItem_Result = objSession.RegisterItems(objOItem_Session, objectList, True)

                                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                    objOItem_Session = Nothing
                                    MsgBox("Beim registrieren der Objekte ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                                End If
                            Else
                                MsgBox("Beim registrieren der Objekte ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                            End If


                        Else
                            MsgBox("Die Elemente in der Beziehung sind kein Objekte. Bitte wählen Sie nur Objekte.", MsgBoxStyle.Information)

                        End If

                    Else
                        If objSession Is Nothing Then
                            objSession = New clsSession(objLocalConfig)
                        End If
                        Dim objectList = DataGridView_Items.SelectedRows.Cast(Of DataGridViewRow).Select(Function(dgvr) CType(dgvr.DataBoundItem, clsObjectRel)).Select(Function(drv) New clsOntologyItem With {
                                                                                                                                                                        .GUID = drv.ID_Object, _
                                                                                                                                                                        .Name = drv.Name_Object, _
                                                                                                                                                                        .GUID_Parent = drv.ID_Parent_Object, _
                                                                                                                                                                        .Type = objLocalConfig.Globals.Type_Object}).ToList()

                        objOItem_Session = objSession.RegisterSession()

                        If Not objOItem_Session Is Nothing Then
                            Dim objOItem_Result = objSession.RegisterItems(objOItem_Session, objectList, True)

                            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                                objOItem_Session = Nothing
                                MsgBox("Beim registrieren der Objekte ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                            End If
                        Else
                            MsgBox("Beim registrieren der Objekte ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
                        End If
                    End If

                End If
            End If

        End If



        If Not OpenLastModuleToolStripMenuItem.Checked Or String.IsNullOrEmpty(strLastModule) Then
            objFrm_Modules = New frmModules(objLocalConfig.Globals, objOItem_Object)
            objFrm_Modules.ShowDialog(Me)
            If objFrm_Modules.DialogResult = DialogResult.OK Then
                Dim strModule = objFrm_Modules.Selected_Module
                Dim strModuleGuid = objFrm_Modules.Selected_ModuleId
                Dim objOItemResult = objLocalConfig.Globals.LState_Nothing.Clone()
                If Not String.IsNullOrEmpty(strModuleGuid) Then
                    If Not objOItem_Object Is Nothing Then
                        objOItemResult = ModuleDataExchanger.Client(strModuleGuid, "Item=" & objOItem_Object.GUID + ",Object")

                        If objOItemResult.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Es konnte keine Instanz des Moduls gefunden werden!", MsgBoxStyle.Exclamation)
                        End If
                    ElseIf Not objOItem_Session Is Nothing Then
                        objOItemResult = ModuleDataExchanger.Client(strModuleGuid, "Session=" & objOItem_Session.GUID)

                        If objOItemResult.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                            MsgBox("Es konnte keine Instanz des Moduls gefunden werden!", MsgBoxStyle.Exclamation)
                        End If
                    End If

                End If
                If objOItemResult.GUID = objLocalConfig.Globals.LState_Nothing.GUID Or objOItemResult.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                    If Not strModule Is Nothing Then
                        objShellWork = New clsShellWork()
                        If Not objOItem_Object Is Nothing Then
                            If objShellWork.start_Process(strModule, "Item=" & objOItem_Object.GUID + ",Object", IO.Path.GetDirectoryName(strModule), False, False) Then
                                strLastModule = strModule
                                OpenLastModuleToolStripMenuItem.ToolTipText = strLastModule
                            Else
                                MsgBox("Das Module konnte nicht gestartet werden!", MsgBoxStyle.Exclamation)
                            End If
                        ElseIf Not objOItem_Session Is Nothing Then
                            If objShellWork.start_Process(strModule, "Session=" & objOItem_Session.GUID, IO.Path.GetDirectoryName(strModule), False, False) Then
                                strLastModule = strModule
                                OpenLastModuleToolStripMenuItem.ToolTipText = strLastModule
                            Else
                                MsgBox("Das Module konnte nicht gestartet werden!", MsgBoxStyle.Exclamation)
                            End If

                        End If

                    End If
                End If
                
            End If
        Else
            objShellWork = New clsShellWork()
            If Not objShellWork.start_Process(strLastModule, "Item=" & objOItem_Object.GUID + ",Object", IO.Path.GetDirectoryName(strLastModule), False, False) Then
                MsgBox("Das Module konnte nicht gestartet werden!", MsgBoxStyle.Exclamation)
            End If

        End If


    End Sub

    Protected Overrides Sub Finalize()
        objDBLevel = Nothing
        objDBLevel2 = Nothing
        objDBLevel3 = Nothing
        objDBLevel4 = Nothing
        objDBLevel5 = Nothing

        oList_Selected_Simple = Nothing
        oList_Selected_ObjectRel = Nothing

        oList_Selected_Simple = Nothing

        MyBase.Finalize()
    End Sub

    Private Sub ToolStripMenuItem_ChooseModule_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem_ChooseModule.Click
        ToolStripMenuItem_ChooseModule.Checked = False
        strNameApplyModule = ""
        strGuidApplyModule = ""
        ToolStripMenuItem_ChooseModule.Text = strTextToolstripApplyModule

        objFrm_Modules = New frmModules(objLocalConfig.Globals, Nothing)
        objFrm_Modules.ShowDialog(Me)
        If objFrm_Modules.DialogResult = DialogResult.OK Then
            strNameApplyModule = objFrm_Modules.Selected_ModuleShort
            strGuidApplyModule = objFrm_Modules.Selected_ModuleId

            ToolStripMenuItem_ChooseModule.Text = strNameApplyModule
            ToolStripMenuItem_ChooseModule.Checked = True
        End If

    End Sub
End Class

