﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AuthenticationForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label_UserName = New System.Windows.Forms.Label()
        Me.TextBox_UserName = New System.Windows.Forms.TextBox()
        Me.Label_Password = New System.Windows.Forms.Label()
        Me.TextBox_Password = New System.Windows.Forms.TextBox()
        Me.Button_Cancel = New System.Windows.Forms.Button()
        Me.Button_OK = New System.Windows.Forms.Button()
        Me.CheckBoxSave = New System.Windows.Forms.CheckBox()
        Me.TextBoxUrl = New System.Windows.Forms.TextBox()
        Me.LabelUrl = New System.Windows.Forms.Label()
        Me.TextBox_GroupName = New System.Windows.Forms.TextBox()
        Me.Label_Group = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label_UserName
        '
        Me.Label_UserName.AutoSize = True
        Me.Label_UserName.Location = New System.Drawing.Point(12, 37)
        Me.Label_UserName.Name = "Label_UserName"
        Me.Label_UserName.Size = New System.Drawing.Size(76, 13)
        Me.Label_UserName.TabIndex = 0
        Me.Label_UserName.Text = "Email-Address:"
        '
        'TextBox_UserName
        '
        Me.TextBox_UserName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_UserName.Location = New System.Drawing.Point(94, 34)
        Me.TextBox_UserName.Name = "TextBox_UserName"
        Me.TextBox_UserName.Size = New System.Drawing.Size(216, 20)
        Me.TextBox_UserName.TabIndex = 2
        '
        'Label_Password
        '
        Me.Label_Password.AutoSize = True
        Me.Label_Password.Location = New System.Drawing.Point(12, 89)
        Me.Label_Password.Name = "Label_Password"
        Me.Label_Password.Size = New System.Drawing.Size(56, 13)
        Me.Label_Password.TabIndex = 4
        Me.Label_Password.Text = "Password:"
        '
        'TextBox_Password
        '
        Me.TextBox_Password.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_Password.Location = New System.Drawing.Point(94, 86)
        Me.TextBox_Password.Name = "TextBox_Password"
        Me.TextBox_Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBox_Password.Size = New System.Drawing.Size(216, 20)
        Me.TextBox_Password.TabIndex = 5
        '
        'Button_Cancel
        '
        Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button_Cancel.Location = New System.Drawing.Point(235, 154)
        Me.Button_Cancel.Name = "Button_Cancel"
        Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
        Me.Button_Cancel.TabIndex = 6
        Me.Button_Cancel.Text = "Cancel"
        Me.Button_Cancel.UseVisualStyleBackColor = True
        '
        'Button_OK
        '
        Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button_OK.Location = New System.Drawing.Point(154, 154)
        Me.Button_OK.Name = "Button_OK"
        Me.Button_OK.Size = New System.Drawing.Size(75, 23)
        Me.Button_OK.TabIndex = 6
        Me.Button_OK.Text = "OK"
        Me.Button_OK.UseVisualStyleBackColor = True
        '
        'CheckBoxSave
        '
        Me.CheckBoxSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxSave.AutoSize = True
        Me.CheckBoxSave.Location = New System.Drawing.Point(15, 158)
        Me.CheckBoxSave.Name = "CheckBoxSave"
        Me.CheckBoxSave.Size = New System.Drawing.Size(129, 17)
        Me.CheckBoxSave.TabIndex = 7
        Me.CheckBoxSave.Text = "Save Connectiondata"
        Me.CheckBoxSave.UseVisualStyleBackColor = True
        '
        'TextBoxUrl
        '
        Me.TextBoxUrl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBoxUrl.Location = New System.Drawing.Point(94, 6)
        Me.TextBoxUrl.Name = "TextBoxUrl"
        Me.TextBoxUrl.Size = New System.Drawing.Size(216, 20)
        Me.TextBoxUrl.TabIndex = 9
        '
        'LabelUrl
        '
        Me.LabelUrl.AutoSize = True
        Me.LabelUrl.Location = New System.Drawing.Point(12, 9)
        Me.LabelUrl.Name = "LabelUrl"
        Me.LabelUrl.Size = New System.Drawing.Size(23, 13)
        Me.LabelUrl.TabIndex = 8
        Me.LabelUrl.Text = "Url:"
        '
        'TextBox_GroupName
        '
        Me.TextBox_GroupName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_GroupName.Location = New System.Drawing.Point(94, 60)
        Me.TextBox_GroupName.Name = "TextBox_GroupName"
        Me.TextBox_GroupName.Size = New System.Drawing.Size(216, 20)
        Me.TextBox_GroupName.TabIndex = 11
        '
        'Label_Group
        '
        Me.Label_Group.AutoSize = True
        Me.Label_Group.Location = New System.Drawing.Point(12, 63)
        Me.Label_Group.Name = "Label_Group"
        Me.Label_Group.Size = New System.Drawing.Size(39, 13)
        Me.Label_Group.TabIndex = 10
        Me.Label_Group.Text = "Group:"
        '
        'AuthenticationForm
        '
        Me.AcceptButton = Me.Button_OK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Button_Cancel
        Me.ClientSize = New System.Drawing.Size(322, 183)
        Me.Controls.Add(Me.TextBox_GroupName)
        Me.Controls.Add(Me.Label_Group)
        Me.Controls.Add(Me.TextBoxUrl)
        Me.Controls.Add(Me.LabelUrl)
        Me.Controls.Add(Me.CheckBoxSave)
        Me.Controls.Add(Me.Button_OK)
        Me.Controls.Add(Me.Button_Cancel)
        Me.Controls.Add(Me.TextBox_Password)
        Me.Controls.Add(Me.Label_Password)
        Me.Controls.Add(Me.TextBox_UserName)
        Me.Controls.Add(Me.Label_UserName)
        Me.Name = "AuthenticationForm"
        Me.Text = "x_Authentication"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label_UserName As Label
    Friend WithEvents TextBox_UserName As TextBox
    Friend WithEvents Label_Password As Label
    Friend WithEvents TextBox_Password As TextBox
    Friend WithEvents Button_Cancel As Button
    Friend WithEvents Button_OK As Button
    Friend WithEvents CheckBoxSave As CheckBox
    Friend WithEvents TextBoxUrl As TextBox
    Friend WithEvents LabelUrl As Label
    Friend WithEvents TextBox_GroupName As TextBox
    Friend WithEvents Label_Group As Label
End Class
