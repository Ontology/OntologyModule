﻿Imports OntologyAppDBConnector
Imports OntologyClasses.BaseClasses
Imports OntologyClasses.DataClasses
Imports Structure_Module

Public Class frmConfig
    Dim strMessage As String
    Dim configItems As List(Of AppConfigItem)
    Dim strPathConfig As String
    Private objResult As clsOntologyItem
    Private objLogStates As New clsLogStates

    Public ReadOnly Property Result As clsOntologyItem
        Get
            Return objResult
        End Get
    End Property

    Public Sub New(strPathConfig As String, Optional message As String = "-")

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        strMessage = message
        ToolStripLabel_Message.Text = strMessage

        objResult = objLogStates.LogState_Nothing.Clone()
        Me.strPathConfig = strPathConfig

        Initialize()
    End Sub

    Private Sub Initialize()
        If AppConfiguration.ReadConfig(strPathConfig) Then

            DataGridView_Config.DataSource = New SortableBindingList(Of AppConfigItem)(AppConfiguration.ConfigItems.OrderBy(Function(ci) ci.ConfigItem).ToList())

            DataGridView_Config.Columns(0).ReadOnly = True
            DataGridView_Config.Columns(2).Visible = False
        Else
            MsgBox("Die Konfiguration kann nicht gelesen werden", MsgBoxStyle.Exclamation)
            objResult = objLogStates.LogState_Error.Clone()
        End If



    End Sub

    Private Sub ToolStripButton_Close_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Close.Click
        objResult = objLogStates.LogState_Nothing.Clone()
        Me.Close()
    End Sub

    Private Sub ToolStripButton_Save_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Save.Click
        If Not IO.File.Exists(strPathConfig) Then
            strPathConfig = Application.StartupPath & IO.Path.DirectorySeparatorChar & strPathConfig
        End If
        If AppConfiguration.ConfigItems.Any(Function(ci) ci.IsModified()) Then
            If AppConfiguration.WriteConfig() Then
                objResult = objLogStates.LogState_Success.Clone()
                Me.Close()
            Else
                MsgBox("Die Konfiguration kann nicht geschrieben werden", MsgBoxStyle.Exclamation)
                objResult = objLogStates.LogState_Error.Clone()
            End If
        Else
            objResult = objLogStates.LogState_Success.Clone()

            Me.Close()
        End If


    End Sub
End Class