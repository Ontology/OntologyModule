﻿Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector
Imports OntologyClassEdit

Public Class frmClassEdit
    Private objLocalConfig As clsLocalConfig
    Private WithEvents objUserControl_ClassEdit As UserControl_ClassEdit
    Private objDlgAttributeLong As dlg_Attribute_Long
    Private objFrmGraph As frmGraph
    Private objFrmMain As frmMain

    Private objClassItem As clsOntologyItem

    Public ReadOnly Property OItem_Class As clsOntologyItem
        Get
            Return objClassItem
        End Get
    End Property

    Public Sub New(ByVal LocalConfig As clsLocalConfig, ByVal oItem_Class As clsOntologyItem)

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        objClassItem = oItem_Class
        objLocalConfig = LocalConfig
        initialize()
    End Sub

    Private Sub initialize()
        objUserControl_ClassEdit = New UserControl_ClassEdit(objLocalConfig.Globals)
        objUserControl_ClassEdit.Dock = DockStyle.Fill
        Controls.Add(objUserControl_ClassEdit)

        objUserControl_ClassEdit.Initialize_ClassEdit(OItem_Class)
    End Sub

    Private Sub OpenGraph(objClassAttributes As List(Of clsClassAtt),
                          objClassRelations_Conscious As List(Of clsClassRel),
                          objClassRelations_SubConscious As List(Of clsClassRel),
                          objClassRelations_Other As List(Of clsClassRel)) Handles objUserControl_ClassEdit._openGraph

        objFrmGraph = New frmGraph(objLocalConfig)
        objFrmGraph.Initialize_Lists()
        objFrmGraph.OList_Classes.Add(objClassItem)

        If (Not objClassAttributes Is Nothing) Then
            objFrmGraph.ClassAtts = objClassAttributes
        End If

        If (Not objClassRelations_Conscious Is Nothing) Then
            Dim objClassRelations = From objClassRelation In objClassRelations_Conscious
                                    Group By objClassRelation.ID_Class_Right, objClassRelation.Name_Class_Right, objLocalConfig.Globals.Type_Class Into objClassRelations1 = Group
                                    Select New clsOntologyItem With {.GUID = ID_Class_Right,
                                        .Name = Name_Class_Right,
                                        .Type = objLocalConfig.Globals.Type_Class}

            objFrmGraph.OList_Classes.AddRange(From objItemNew In objClassRelations
                                               Group Join objItemIn In objFrmGraph.OList_Classes On objItemNew.GUID Equals objItemIn.GUID Into objItemsIn = Group
                                               From objItemIn In objItemsIn
                                               Where objItemIn Is Nothing
                                               Select objItemNew)

            objFrmGraph.OList_ClassRel = objClassRelations_Conscious
        End If

        If (Not objClassRelations_SubConscious Is Nothing) Then
            Dim objClassRelations = From objClassRelation In objClassRelations_SubConscious
                                    Group By objClassRelation.ID_Class_Left, objClassRelation.Name_Class_Left, objLocalConfig.Globals.Type_Class Into objClassRelations1 = Group
                                    Select New clsOntologyItem With {.GUID = ID_Class_Left,
                                        .Name = Name_Class_Left,
                                        .Type = objLocalConfig.Globals.Type_Class}

            objFrmGraph.OList_Classes.AddRange(From objItemNew In objClassRelations
                                               Group Join objItemIn In objFrmGraph.OList_Classes On objItemNew.GUID Equals objItemIn.GUID Into objItemsIn = Group
                                               From objItemIn In objItemsIn
                                               Where objItemIn Is Nothing
                                               Select objItemNew)

            objFrmGraph.OList_ClassRel = objClassRelations_Conscious
        End If

        If (Not objClassRelations_Other Is Nothing) Then
            objFrmGraph.OList_ClassRel.AddRange(objClassRelations_Other)
        End If

        objFrmGraph.Initialize_ListGraph()
        objFrmGraph.Show()
    End Sub

    Private Sub GetRelationType(strSessionId As String) Handles objUserControl_ClassEdit._getRelationType
        objFrmMain = New frmMain(objLocalConfig, objLocalConfig.Globals.Type_RelationType)
        objFrmMain.ShowDialog(Me)

        If objFrmMain.DialogResult = DialogResult.OK Then
            objUserControl_ClassEdit.SetRelationType(objFrmMain.OList_Simple.FirstOrDefault(), strSessionId)
        End If
    End Sub

    Private Sub GetClass(strSessionId) Handles objUserControl_ClassEdit._getClass
        objFrmMain = New frmMain(objLocalConfig, objLocalConfig.Globals.Type_Class)
        objFrmMain.ShowDialog(Me)

        If objFrmMain.DialogResult = DialogResult.OK Then
            objUserControl_ClassEdit.SetClass(objFrmMain.OList_Simple.FirstOrDefault(), strSessionId)
        End If
    End Sub

    Private Sub GetLongValue(strSessionId As String, lngValue As Long) Handles objUserControl_ClassEdit._getLongValue
        objDlgAttributeLong = New dlg_Attribute_Long("Get Value", objLocalConfig, lngValue)
        objDlgAttributeLong.ShowDialog(Me)

        If objDlgAttributeLong.DialogResult = DialogResult.OK Then
            objUserControl_ClassEdit.SetLongValue(strSessionId, objDlgAttributeLong.Value)
        End If

    End Sub

    Private Sub GetAttributeType(strSessionId As String) Handles objUserControl_ClassEdit._getAttributeType
        objFrmMain = New frmMain(objLocalConfig, objLocalConfig.Globals.Type_AttributeType)
        objFrmMain.ShowDialog(Me)

        If objFrmMain.DialogResult = DialogResult.OK Then
            objUserControl_ClassEdit.SetSimpleItemListAttributeType(objFrmMain.Type_Applied, objFrmMain.OList_Simple, strSessionId)
        End If
    End Sub

    Private Sub DelClass() Handles objUserControl_ClassEdit._deletedItem
        Me.Close()
    End Sub
End Class