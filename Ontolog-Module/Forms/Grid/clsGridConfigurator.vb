﻿Imports Nest
Imports OntologyClasses.BaseClasses
Public Class clsGridConfigurator
    Public Sub ConfigureGruid(objDgv As DataGridView, objTypeItem As Type, Optional boolLeftRight As Nullable(Of Boolean) = Nothing, Optional boolOrderIdFirst As Boolean = False)

        If objTypeItem = GetType(clsOntologyItem) Then
            If (Not objDgv.Columns Is Nothing) Then
                For Each o As DataGridViewColumn In objDgv.Columns
                    If o.DataPropertyName.ToLower() = "Name".ToLower() Then

                        o.Width = 300

                        o.Visible = True
                    Else
                        o.Visible = False
                    End If
                Next
            End If
        ElseIf objTypeItem = GetType(clsObjectAtt) Then
            If Not objDgv.Columns Is Nothing Then

                For Each o As DataGridViewColumn In objDgv.Columns
                    If o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Or _
                       o.DataPropertyName.ToLower() = "Name_Class".ToLower() Or _
                       o.DataPropertyName.ToLower() = "Name_Object".ToLower() Or _
                       o.DataPropertyName.ToLower() = "Val_Named".ToLower() Or _
                       o.DataPropertyName.ToLower() = "Name_DataType".ToLower() Or _
                       o.DataPropertyName.ToLower() = "OrderID".ToLower() Then

                        If o.DataPropertyName.ToLower() = "Name_Class".ToLower() Then
                            o.Width = 50
                        End If
                        If o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Then
                            o.Width = 50
                        End If
                        If o.DataPropertyName.ToLower() = "Name_Object".ToLower() Then
                            o.Width = 100
                        End If
                        If o.DataPropertyName.ToLower() = "Val_Named".ToLower() Then
                            o.Width = 100
                        End If

                        o.Visible = True
                    Else
                        o.Visible = False
                    End If
                Next
                objDgv.Columns("OrderID").DisplayIndex = 0
            End If

        ElseIf objTypeItem = GetType(clsObjectRel) Then
            If (Not objDgv.Columns Is Nothing) Then
                objDgv.Columns("OrderID").DisplayIndex = 0
                
                For Each o As DataGridViewColumn In objDgv.Columns

                    If boolLeftRight Is Nothing Then
                        If o.DataPropertyName.ToLower() = "Name_Object".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_Parent_Object".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_Other".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_Parent_Other".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_RelationType".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Ontology".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_Direction".ToLower() Or _
                           o.DataPropertyName.ToLower() = "OrderID".ToLower() Then
                            o.Visible = True
                        Else
                            o.Visible = False
                        End If


                    ElseIf boolLeftRight Then
                        If o.DataPropertyName.ToLower() = "Name_Other".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_Parent_Other".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_RelationType".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Ontology".ToLower() Or _
                           o.DataPropertyName.ToLower() = "OrderID".ToLower() Then
                            o.Visible = True
                        Else
                            o.Visible = False
                        End If

                    Else
                        If o.DataPropertyName.ToLower() = "Name_Object".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_Parent_Object".ToLower() Or _
                           o.DataPropertyName.ToLower() = "Name_RelationType".ToLower() Or _
                           o.DataPropertyName.ToLower() = "OrderID".ToLower() Then
                            o.Visible = True
                        Else
                            o.Visible = False
                        End If
                    End If

                Next
            End If
        ElseIf objTypeItem = GetType(clsClassAtt) Then
            If (Not objDgv.Columns Is Nothing) Then

                For Each o As DataGridViewColumn In objDgv.Columns

                    If boolLeftRight Is Nothing Then
                        If o.DataPropertyName.ToLower() = "Name_Class".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Name_DataType".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Min".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Max".ToLower() Then

                            If o.DataPropertyName.ToLower() = "Name_Class".ToLower() Then
                                o.Width = 100
                            End If

                            If o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Then
                                o.Width = 100
                            End If
                            o.Visible = True
                        Else
                            o.Visible = False
                        End If
                    ElseIf boolLeftRight Then
                        If o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Name_DataType".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Min".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Max".ToLower() Then
                            If o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Then
                                o.Width = 100
                            End If
                            o.Visible = True
                        Else
                            o.Visible = False
                        End If
                    Else
                        If o.DataPropertyName.ToLower() = "Name_Class".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Name_DataType".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Min".ToLower() Or _
                            o.DataPropertyName.ToLower() = "Max".ToLower() Then

                            If o.DataPropertyName.ToLower() = "Name_Class".ToLower() Then
                                o.Width = 100
                            End If
                            If o.DataPropertyName.ToLower() = "Name_AttributeType".ToLower() Then
                                o.Width = 100
                            End If
                            o.Visible = True
                        Else
                            o.Visible = False
                        End If
                    End If

                Next
            End If
        End If
    End Sub
End Class
