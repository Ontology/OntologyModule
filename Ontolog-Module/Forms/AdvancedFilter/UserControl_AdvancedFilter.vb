﻿Imports System.Reflection
Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices

Public Class UserControl_AdvancedFilter
    Private objLocalConfig As clsLocalConfig

    Private objOItem_Object As clsOntologyItem
    Private objOItem_Class As clsOntologyItem
    Private objOItem_RelationType As clsOntologyItem

    Private objFrm_OntologyModule As frmMain

    Public Event AddItems()
    Public Event Applyable()
    Public Event NotApplyable()

    Public ReadOnly Property NullRelation As Boolean
        Get
            Return ToolStripButton_Null.Checked
        End Get
    End Property

    Public Property OItem_Class() As clsOntologyItem
        Get
            Return objOItem_Class
        End Get
        Set(value As clsOntologyItem)
            objOItem_Class = value
            ToolStripButton_AddRelations.Enabled = False
        End Set
    End Property

    Public Property OItem_RelationType As clsOntologyItem
        Get
            Return objOItem_RelationType
        End Get
        Set(value As clsOntologyItem)
            objOItem_RelationType = value
            ToolStripButton_AddRelations.Enabled = True
            Configure_Controls()
        End Set
    End Property

    Public ReadOnly Property OItem_Object As clsOntologyItem
        Get
            Return objOItem_Object
        End Get
    End Property

    Public Sub EnableAdd()
        ToolStripButton_AddRelations.Enabled = True
    End Sub

    Public Sub New(LocalConfig As clsLocalConfig)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfig
        Initialize()
    End Sub

    Public Sub New(Globals As Globals)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        Initialize()
    End Sub

    Public Sub Initialize_Relation(OItem_Class As clsOntologyItem, OItem_RelationType As clsOntologyItem)
        RaiseEvent NotApplyable()
        objOItem_Class = OItem_Class
        objOItem_RelationType = OItem_RelationType
        objOItem_Object = Nothing

        CleanControls()

        Configure_Controls()

    End Sub

    Private Sub Initialize()
        CleanControls()
    End Sub

    Public Sub Configure_Controls()
        objOItem_Object = Nothing

        TextBox_Objects.Text = ""
        Button_RemoveObject.Enabled = False

        If Not objOItem_Class Is Nothing Then
            TextBox_Class.Text = objOItem_Class.Name
            Button_RemoveClass.Enabled = True
        Else
            TextBox_Class.Text = ""
            Button_RemoveClass.Enabled = False
        End If

        If Not objOItem_RelationType Is Nothing Then
            TextBox_RelationType.Text = objOItem_RelationType.Name
            Button_RemoveRelationType.Enabled = True
        Else
            TextBox_RelationType.Text = ""
            Button_RemoveRelationType.Enabled = False
        End If

        If MsgBox("Wollen Sie ein Objekt der Klasse auswählen?", MessageBoxButtons.YesNo, "Objekt auswählen?") = MsgBoxResult.Yes Then
            objFrm_OntologyModule = New frmMain(objLocalConfig, objLocalConfig.Globals.Type_Class, objOItem_Class)
            objFrm_OntologyModule.ShowDialog(Me)
            If objFrm_OntologyModule.DialogResult = DialogResult.OK Then
                If objFrm_OntologyModule.Type_Applied = objLocalConfig.Globals.Type_Object Then
                    If objFrm_OntologyModule.OList_Simple.Count = 1 Then
                        objOItem_Object = objFrm_OntologyModule.OList_Simple(0)
                        If Not objOItem_Object.GUID_Parent = objOItem_Class.GUID Then
                            MsgBox("Wählen Sie bitte ein Object vom Typ " & objOItem_Class.Name & " auswählen!", MsgBoxStyle.Information)
                            objOItem_Object = Nothing
                        Else
                            TextBox_Objects.Text = objOItem_Object.Name
                        End If
                    Else
                        MsgBox("Wählen Sie bitte ein Object vom Typ " & objOItem_Class.Name & " auswählen!", MsgBoxStyle.Information)
                    End If
                Else
                    MsgBox("Wählen Sie bitte ein Object vom Typ " & objOItem_Class.Name & " auswählen!", MsgBoxStyle.Information)
                End If
            End If
        End If
        If Not objOItem_Class Is Nothing And Not objOItem_RelationType Is Nothing Then
            RaiseEvent Applyable()
        End If
    End Sub

    Private Sub CleanControls()
        ToolStripButton_AddRelations.Enabled = False
        TextBox_Class.Text = ""
        TextBox_Objects.Text = ""
        TextBox_RelationType.Text = ""
        Button_RemoveClass.Enabled = False
        Button_RemoveObject.Enabled = False
        Button_RemoveRelationType.Enabled = False
    End Sub

    Private Sub ToolStripButton_AddRelations_Click(sender As Object, e As EventArgs) Handles ToolStripButton_AddRelations.Click
        RaiseEvent AddItems()
    End Sub
End Class
