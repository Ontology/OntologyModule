﻿Imports ClassLibrary_ShellWork
Imports OntologyClasses.BaseClasses

Public Class clsModuleSelectionWorker
    Private objFrmModules As frmModules
    Private objShell As New clsShellWork

    Public Property OItemToSend As clsOntologyItem
    Public Property OpenLastModule As Boolean
    Public Property LastModuleName As String


    Public Function OpenModuleWithArgument(objOItemToSend As clsOntologyItem, objParentForm As IWin32Window, objGlobals As OntologyAppDBConnector.Globals) As clsOntologyItem
        OItemToSend = objOItemToSend
        If OpenLastModule = True And Not String.IsNullOrEmpty(LastModuleName) Then
            If objShell.start_Process(LastModuleName, "Item=" & OItemToSend.GUID & ",Object",
                                              System.IO.Path.GetDirectoryName(LastModuleName), False, False) Then


            Else
                MessageBox.Show(objParentForm, "Das Modul konnte nicht gestartet werden!", "Fehler!",
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return objGlobals.LState_Error.Clone()
            End If
        Else
            objFrmModules = New frmModules(objGlobals, objOItemToSend)
            objFrmModules.ShowDialog(objParentForm)

            If objFrmModules.DialogResult = DialogResult.OK Then
                Dim strModule = objFrmModules.Selected_Module
                If Not String.IsNullOrEmpty(strModule) Then
                    If objShell.start_Process(strModule, "Item=" & OItemToSend.GUID & ",Object",
                                              System.IO.Path.GetDirectoryName(strModule), False, False) Then

                        LastModuleName = strModule
                    Else
                        MessageBox.Show(objParentForm, "Das Modul konnte nicht gestartet werden!", "Fehler!",
                                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return objGlobals.LState_Error.Clone()
                    End If

                End If

            End If


        End If


        Return objGlobals.LState_Success.Clone()
    End Function

    Public Sub New()
    End Sub
End Class
