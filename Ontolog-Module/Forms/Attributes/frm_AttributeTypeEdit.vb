﻿Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector

Public Class frm_AttributeTypeEdit

    Private objLocalConfig As clsLocalConfig

    Private objDBLevel As OntologyModDBConnector

    Private WithEvents objUserControl_AttributeTypeSel As UserControl_AttributeTypeSel

    Private objOItem_AttributeType As clsOntologyItem

    Private strDType As String

    Private boolOpen As Boolean

    Private Sub changed_DType(ByVal strType As String) Handles objUserControl_AttributeTypeSel.selected_Type
        save_DataType(strType)
    End Sub

    Public Sub New(ByVal LocalConfig As clsLocalConfig, ByVal OItem_AttributeType As clsOntologyItem)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        objLocalConfig = LocalConfig
        objOItem_AttributeType = OItem_AttributeType

        set_DBConnection()
        initialize()
    End Sub

    Private Sub initialize()
        boolOpen = True
        GetDataAttributeType()
        If boolOpen = True Then
            ToolStripTextBox_GUID.Text = objOItem_AttributeType.GUID
            ToolStripTextBox_Name.ReadOnly = True
            ToolStripTextBox_Name.Text = objOItem_AttributeType.Name
            ToolStripTextBox_Name.ReadOnly = False

            objUserControl_AttributeTypeSel = New UserControl_AttributeTypeSel(strDType, objLocalConfig)
            objUserControl_AttributeTypeSel.Dock = DockStyle.Fill
            SplitContainer1.Panel1.Controls.Add(objUserControl_AttributeTypeSel)
        End If
    End Sub

    Private Sub GetDataAttributeType()
        Dim oList_AttributeTypes As New List(Of clsOntologyItem)

        oList_AttributeTypes.Add(objOItem_AttributeType)
        objDBLevel.GetDataAttributeType(oList_AttributeTypes)

        objOItem_AttributeType = objDBLevel.AttributeTypes.FirstOrDefault()
        If Not objOItem_AttributeType Is Nothing Then
            
            Select Case objOItem_AttributeType.GUID_Parent
                Case objLocalConfig.Globals.DType_Bool.GUID
                    strDType = objLocalConfig.Globals.DType_Bool.Name
                Case objLocalConfig.Globals.DType_DateTime.GUID
                    strDType = objLocalConfig.Globals.DType_DateTime.Name
                Case objLocalConfig.Globals.DType_Int.GUID
                    strDType = objLocalConfig.Globals.DType_Int.Name

                Case objLocalConfig.Globals.DType_Real.GUID
                    strDType = objLocalConfig.Globals.DType_Real.Name
                Case objLocalConfig.Globals.DType_String.GUID
                    strDType = objLocalConfig.Globals.DType_String.Name
            End Select

        Else
            MsgBox("Der Attributtype ist nicht vorhanden!", MsgBoxStyle.Exclamation)
            boolOpen = False
        End If
    End Sub


    Private Sub set_DBConnection()
        objDBLevel = New OntologyModDBConnector(objLocalConfig.Globals)

    End Sub

    Private Sub frm_AttributeTypeEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If boolOpen = False Then
            Me.Close()
        End If
    End Sub

    Private Sub save_DataType(ByVal strType As String)
        Dim objOItem_Result As clsOntologyItem

        Select Case strType
            Case objLocalConfig.Globals.DType_Bool.Name
                objOItem_AttributeType.GUID_Parent = objLocalConfig.Globals.DType_Bool.GUID
            Case objLocalConfig.Globals.DType_DateTime.Name
                objOItem_AttributeType.GUID_Parent = objLocalConfig.Globals.DType_DateTime.GUID
            Case objLocalConfig.Globals.DType_Int.Name
                objOItem_AttributeType.GUID_Parent = objLocalConfig.Globals.DType_Int.GUID

            Case objLocalConfig.Globals.DType_Real.Name
                objOItem_AttributeType.GUID_Parent = objLocalConfig.Globals.DType_Real.GUID

            Case objLocalConfig.Globals.DType_String.Name
                objOItem_AttributeType.GUID_Parent = objLocalConfig.Globals.DType_String.GUID


        End Select

        objOItem_Result = objDBLevel.SaveAttributeTypes(New List(Of clsOntologyItem) From {objOItem_AttributeType})
    End Sub

    Private Sub ToolStripTextBox_Name_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox_Name.TextChanged
        Timer_Name.Stop()
        If Not ToolStripTextBox_Name.ReadOnly Then
            Timer_Name.Start()
        End If

    End Sub

    Private Sub Timer_Name_Tick(sender As Object, e As EventArgs) Handles Timer_Name.Tick
        Timer_Name.Stop()
        If Not String.IsNullOrEmpty(ToolStripTextBox_Name.Text) Then
            objOItem_AttributeType.Name = ToolStripTextBox_Name.Text
            Dim objOItem_Result = objDBLevel.SaveAttributeTypes(New List(Of clsOntologyItem) From {objOItem_AttributeType})
            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                MsgBox("Beim Speichern ist ein Fehler aufgetreten!", MsgBoxStyle.Exclamation)
            End If
        Else
            MsgBox("Der Attributtyp muss eine Bezeichnung tragen!", MsgBoxStyle.Information)
            ToolStripTextBox_Name.ReadOnly = True
            ToolStripTextBox_Name.Text = objOItem_AttributeType.Name
            ToolStripTextBox_Name.ReadOnly = False
        End If
    End Sub
End Class