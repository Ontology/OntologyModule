﻿Imports System.Configuration

Public Class AuthenticationForm

    Public ReadOnly Property UserName As String
        Get
            Return TextBox_UserName.Text
        End Get
    End Property

    Public ReadOnly Property Password As String
        Get
            Return TextBox_Password.Text
        End Get
    End Property


    Public ReadOnly Property Group As String
        Get
            Return TextBox_GroupName.Text
        End Get
    End Property

    Private Sub Button_OK_Click(sender As Object, e As EventArgs) Handles Button_OK.Click
        If String.IsNullOrEmpty(TextBox_UserName.Text) Or String.IsNullOrEmpty(TextBox_Password.Text) Then
            MsgBox("Enter username and password, please!", MsgBoxStyle.Information)
            Return
        End If

        If CheckBoxSave.Checked Then
            Dim config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Dim sectionGroup = config.GetSectionGroup("userSettings")
            Dim section = sectionGroup.Sections.Cast(Of ConfigurationSection).FirstOrDefault(Function(sec) sec.SectionInformation.Name = "Authentication")
            Dim settings = (CType(section, AppSettingsSection)).Settings

            settings("Username").Value = UserName
            settings("Password").Value = Password
            config.Save()
        End If

        DialogResult = DialogResult.OK
    End Sub

    Private Sub Button_Cancel_Click(sender As Object, e As EventArgs) Handles Button_Cancel.Click
        DialogResult = DialogResult.Cancel
    End Sub
End Class