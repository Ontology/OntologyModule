﻿Imports System.Reflection
Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices

Public Class frmReplace
    Private objLocalConfig As clsLocalConfig

    Private itemList As List(Of clsOntologyItem)

    Private objDBLevel_Save As OntologyModDBConnector

    Private WithEvents objUserControl_Replace As UserControl_Replace

    Private Sub Replaced() Handles objUserControl_Replace.Replaced
        Dim objItemList_NotOk = objUserControl_Replace.ItemList.Where(Function(i) i.Additional1 Is Nothing Or i.Additional1 = "" Or i.Additional1.Length > 255).ToList()

        If Not objItemList_NotOk.Any() Then
            ToolStripButton_Ok.Enabled = True
        Else
            ToolStripButton_Ok.Enabled = False
        End If
    End Sub

    Private Sub Initialize()
        objDBLevel_Save = New OntologyModDBConnector(objLocalConfig.Globals)
        objUserControl_Replace = New UserControl_Replace(itemList)
        objUserControl_Replace.Dock = DockStyle.Fill
        ToolStripContainer1.ContentPanel.Controls.Add(objUserControl_Replace)
    End Sub
    Private Sub ToolStripButton_Close_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Close.Click
        Me.Close()
    End Sub

    Public Sub New(LocalConfig As clsLocalConfig, ItemList As List(Of clsOntologyItem))
        ' This call is required by the designer.
        InitializeComponent()
        objLocalConfig = LocalConfig

        Me.itemList = ItemList
        Initialize()
    End Sub

    Public Sub New(Globals As Globals, ItemList As List(Of clsOntologyItem))
        ' This call is required by the designer.
        InitializeComponent()
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If
        Me.itemList = ItemList
        Initialize()
    End Sub

    Private Sub ToolStripButton_Ok_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Ok.Click
        If objUserControl_Replace.ItemList.Any() Then
            Dim objObjects = objUserControl_Replace.ItemList.Select(Function(o) New clsOntologyItem With {.GUID = o.GUID, _
                                                                                                          .Name = o.Additional1, _
                                                                                                          .GUID_Parent = o.GUID_Parent, _
                                                                                                          .Type = objLocalConfig.Globals.Type_Object}).ToList()
            Dim objOItem_Result = objDBLevel_Save.SaveObjects(objObjects)
            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                MsgBox("Die Items konnten nicht geändert werden!", MsgBoxStyle.Exclamation)
            Else
                Me.DialogResult = DialogResult.OK
                Me.Close()
            End If
        Else
            MsgBox("Die Itemliste ist leer!", MsgBoxStyle.Exclamation)
        End If

    End Sub
End Class